from django.contrib import admin

from .models import Comment

@admin.register(Comment)
class CustomCommentAdmin(admin.ModelAdmin):
    list_display = ('id', 'parent_id', 'created_at', 'author', 'content_type', '__str__', 'is_deleted',)
    list_display_links = ('created_at', 'author', 'content_type', '__str__')
    list_filter = ('created_at', 'author', 'content_type', 'is_deleted',)
    search_fields =  ("author__username", 'content_type', 'text',)
    ordering = ('-created_at', 'author', 'content_type', 'text', 'is_deleted',)
    list_editable = ('is_deleted', )
    raw_id_fields = ('author', 'parent')