from django.http import HttpResponseRedirect
from django.core.exceptions import PermissionDenied
from django.views.generic import View
from django.views.generic.edit import FormMixin
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator
from django.http import Http404

from braces.views import LoginRequiredMixin

from .models import Comment
from .forms import CommentForm


class PostCommentView(LoginRequiredMixin, View, FormMixin):
    """Отправка комментария."""

    form_class = CommentForm

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if hasattr(self, 'object'):
            kwargs.update({'instance': self.object})
        kwargs["author"] = self.request.user
        return kwargs

    def form_valid(self, form):
        self.object = form.save(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        # TODO: при неверной валидации выводить сообщение на странице (пр. popup)
        return HttpResponseRedirect(self.get_success_url())

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

post_comment_view = PostCommentView.as_view()

class DeleteCommentView(LoginRequiredMixin, View):
    """Удаление комментария."""

    def get(self, request, *args, **kwargs):
        comment = get_object_or_404(Comment, pk=kwargs.get('pk', None))
        if request.user == comment.author:
            if not comment.is_leaf_node():
                comment.is_deleted = True
                comment.save()
            else:
                comment.delete()
        else:
            raise PermissionDenied()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

delete_comment_view = DeleteCommentView.as_view()

class DetailViewCommentsPaginatorMixin:
    """Добавляет пагинатор для комментариев на страницу записи."""

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        if not self.object.is_active:
            raise Http404

        paginator = Paginator(self.object.comments.all(), 10)
        page = request.GET.get('page', 1)
        comments = paginator.get_page(page)
        context = self.get_context_data(object=self.object,
            comments=comments, page=paginator.page(page))

        return self.render_to_response(context)