from django import template
from django.template import loader
from django.contrib.contenttypes.models import ContentType

from ..utils import get_attr_val
from ..forms import CommentForm


register = template.Library()

@register.simple_tag()
def get_comment_hidden_fields(content_object, **kwargs):
    """Вставляет в шаблон content_type_id и object_id для комментария."""

    initialize_template = get_attr_val(None, content_object, 'initialize_template', 'comments/initialize.html', **kwargs)
    context = {
        'ct_id': ContentType.objects.get_for_model(content_object).id,
        'obj_id': content_object.id,
    }
    return loader.render_to_string(initialize_template, context)

@register.simple_tag()
def initialize_comments_captcha():
    """Вставляет в шаблон форму капчи."""

    initialize_template = 'comments/comment_captcha.html'
    context = {
        'form': CommentForm,
    }
    return loader.render_to_string(initialize_template, context)