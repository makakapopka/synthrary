from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from mptt.models import MPTTModel, TreeForeignKey

from ..utils.models import TimeStampMixin
from ..users.models import User


class Comment(TimeStampMixin, MPTTModel):
    """Модель комментария."""

    author = models.ForeignKey(User, on_delete=models.SET_NULL, verbose_name='автор',
        related_name='comments', null=True, blank=True
    )

    text = models.TextField(verbose_name='комментарий', max_length=3000, blank=True)

    content_type = models.ForeignKey(ContentType,verbose_name='тип записи',
                                     blank=True, null=True, on_delete=models.CASCADE
    )

    object_id = models.PositiveIntegerField(verbose_name='id записи')
    content_object = GenericForeignKey()

    parent = TreeForeignKey(
        'self',
        verbose_name='родительский комментарий',
        null=True, blank=True, db_index=True, related_name='comment_children', on_delete=models.CASCADE
    )

    is_deleted = models.BooleanField(default=False, verbose_name='удален')

    class MPTTMeta:
        order_insertion_by = 'created_at'

    class Meta:
        get_latest_by = "created_at"
        verbose_name = 'комментарий'
        verbose_name_plural = 'комментарии'

    def __str__(self):
        return self.text[:120]

    @property
    def is_day_passed(self):
        """Проверяет был ли комментарий оставлен сутки назад."""

        import time
        DAY = 86400
        now = time.time()
        then = time.mktime(self.created_at.timetuple())
        
        if (now - then) > DAY:
            return True

