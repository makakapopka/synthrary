from django.urls import path

from . import views


urlpatterns = [
	path('post', views.post_comment_view, name='post_comment'),
	path('delete/<int:pk>/', views.delete_comment_view, name='delete_comment')
]