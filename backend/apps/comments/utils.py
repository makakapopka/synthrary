def get_attr_val(request, obj, attr, default=None, **kwargs):
    """
    This function attempts to get a value from the 'obj' through 'attr' (either a callable or a variable). 
    If 'attr' is not defined on 'obj' then we attempt to fall back to the default.
    """
    if hasattr(obj, attr):
        attr_holder = getattr(obj, attr)
        if callable(attr_holder):
            kwargs['request'] = kwargs.get('request', request)
            kwargs['user'] = kwargs.get('user', request.user)
            return attr_holder(**kwargs)
        return attr_holder
    return default

