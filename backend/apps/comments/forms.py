from django import forms

from .models import Comment
from ..utils.forms import CaptchaField


class CommentForm(forms.ModelForm):
	"""Форма комментария."""

	captcha = CaptchaField

	def __init__(self, author=None, *args, **kwargs):
		self.author = author
		super().__init__(*args, **kwargs)

	def save(self, form):
		self.instance.author = self.author
		self.instance.content_type_id = form.data.get('ct_id', None)
		self.instance.object_id = form.data.get('obj_id', None)
		# parent_id = form.data.get('parent_id', None)
		# if parent_id:
		# 	self.instance.parent_id = parent_id
		self.instance.save()

	class Meta:
		model = Comment
		fields = ('text', 'captcha')