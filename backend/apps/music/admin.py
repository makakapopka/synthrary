from django.contrib import admin
from django.urls import reverse

from .models import Genre, Musician, Release, Label
from . import forms
from ..utils.widgets import TinyMCETextareaAdminMixin
from ..utils.forms import AddFormMixin


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug')
    list_display_links = ('name', 'slug')
    ordering = ('name', 'slug')
    sortable_by = ('name', 'slug')
    search_fields = ('name', 'slug')
    list_filter = ('name', 'slug')
    readonly_fields = ('slug', )

@admin.register(Label)
class LabelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug')
    list_display_links = ('name', 'slug')
    ordering = ('name', 'slug')
    sortable_by = ('name', 'slug')
    search_fields = ('name', 'slug')
    list_filter = ('name', 'slug')
    readonly_fields = ('slug', )

@admin.register(Musician)
class MusicianAdmin(AddFormMixin, TinyMCETextareaAdminMixin, admin.ModelAdmin):
    add_form = forms.MusicianCreationForm
    form = forms.CleanedPictureChangeForm

    list_display = ('id', 'name', 'created_at', 'type', 'releases_number', 'is_active')
    list_display_links = ('name', )
    list_editable = ('is_active', )
    ordering = ('-created_at', 'name', 'type', 'is_active')
    sortable_by = ('created_at', 'name', 'type', 'releases_number', 'is_active')
    search_fields = ('name', 'irl_name', 'country', )
    list_filter = ('created_at', 'name', 'type', 'is_active')
    date_hierarchy = 'created_at'
    readonly_fields = ('slug', )
    raw_id_fields = ('genre',)

    def releases_number(self, obj):
        return obj.releases.count()
    releases_number.short_description = 'Количество релизов'

    def view_on_site(self, obj):
        return obj.get_absolute_url()

    def get_deleted_objects(self, objs, request):
        return super().get_deleted_objects([release for release in objs[0].releases.all()], request)

    fieldsets = (
        ('Общее', {
            'fields': ('name', 'slug', 'picture', 'is_active')
            }
         ),

        ('Информация', {
            'fields': ('irl_name', 'type', 'members', 'gender', 'country', 
                'genre', 'other_projects')
        }),

        ('Контакты', {
            'fields': ('vk_group', 'vk', 'facebook',
                'twitter', 'instagram', 'bandcamp')
        }),

        ('Описание', {
            'fields': ('about',)
        })
    )

@admin.register(Release)
class ReleaseAdmin(AddFormMixin, TinyMCETextareaAdminMixin, admin.ModelAdmin):
    add_form = forms.ReleaseCreationForm
    form = forms.CleanedPictureChangeForm

    list_display = ('id', 'created_at', 'authors', 'title', 'date_released', 'format', 'label', 'number_of_tracks', 'is_active')
    list_display_links = ('created_at', 'authors', 'title')
    list_editable = ('is_active', )
    ordering = ('-created_at', 'title', 'date_released', 'is_active', 'number_of_tracks')
    sortable_by = ('title', 'created_at', 'authors', 'label', 'format', 'date_released', 'is_active', 'number_of_tracks')
    search_fields = ('title', 'tracklist', 'about')
    list_filter = ('created_at', 'title', 'format', 'label')
    date_hierarchy = 'created_at'
    raw_id_fields = ('author', 'genre', 'label', 'related_musicians')

    def authors(self, obj):
        if len(obj.author.all()) == 1:
            return ''.join([str(a) for a in obj.author.all()])
        else:
            return ' & '.join([str(a) for a in obj.author.all()])
    authors.short_description = 'Автор/ы'

    def view_on_site(self, obj):
        return obj.get_absolute_url()

    fieldsets = (
        ('Общее', {
            'fields': ('author', 'title', 'picture', 'date_released', 'is_active')
            }
         ),

        ('Информация', {
            'fields': ('genre', 'label', 'format', 'number_of_tracks', 
                'tracklist', 'related_musicians')
        }),

        ('Ссылки', {
            'fields': ('support', 'listen')
        }),

        ('Описание', {
            'fields': ('about',)
        })
    )