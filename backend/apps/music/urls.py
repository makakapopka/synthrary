from django.urls import path

from . import views

urlpatterns = [

    # Музыкант
    path('musicians/', views.musicians_list_view, name='musicians'),
    path('musicians/<str:slug>/', views.musician_detail_view, name='musician_detail'),

    # Релиз
    path('releases/', views.releases_list_view, name='releases'),
    path('releases/<int:pk>/', views.release_detail_view, name='release_detail'),
]