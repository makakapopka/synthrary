import io

from django import forms
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import UploadedFile
from django.db.models.fields.files import ImageFieldFile

from PIL import Image, ImageOps

from .models import Musician, Release
from ..utils.validators import FileTypeValidator, FileWeightValidator

class CaseInsensitiveNameMixin:
    """
    Disallow a username with a case-insensitive match of existing usernames.
    Add this mixin to any forms that use the User object
    """
    def clean_name(self):
        name = self.cleaned_data.get('name')
        if Musician.objects.filter(name__iexact=name) \
                                   .exclude(pk=self.instance.pk).exists():
            raise forms.ValidationError('Музыкант с таким именем уже существует.', code='unique')
        return name

class MusicianCreationForm(forms.ModelForm, CaseInsensitiveNameMixin):
    """Форма создания музыканта в админке."""

    picture = forms.ImageField(label='Изображение',
        validators=[FileTypeValidator(allowed_types=['image/jpg', 'image/jpeg', 'image/png']),
        FileWeightValidator(weight=2*(1024*1024))], required=False)

    def clean_picture(self):
        pic = self.cleaned_data.get('picture', None)
        if pic is not None and isinstance(pic, UploadedFile):
            root, ext = pic.content_type.split('/')
            picture = Image.open(pic)
            picture = ImageOps.fit(picture, (500, 500), Image.ANTIALIAS, centering=(0.5, 0.5))
            picture_io = io.BytesIO()
            picture.save(picture_io, format=ext)
            picture = ContentFile(picture_io.getvalue(), pic.name)
            return picture
        elif pic is not None and isinstance(pic, ImageFieldFile):
            raise forms.ValidationError(message='Произошла ошибка, попробуйте позже.')

    class Meta:
        fields = ('name', 'picture')

class ReleaseCreationForm(forms.ModelForm):
    """Форма создания релиза в админке."""

    picture = forms.ImageField(label='Изображение',
        validators=[FileTypeValidator(allowed_types=['image/jpg', 'image/jpeg', 'image/png']),
        FileWeightValidator(weight=2*(1024*1024))], required=False)

    def clean_picture(self):
        pic = self.cleaned_data.get('picture', None)
        if pic is not None and isinstance(pic, UploadedFile):
            root, ext = pic.content_type.split('/')
            picture = Image.open(pic)
            picture = ImageOps.fit(picture, (500, 500), Image.ANTIALIAS, centering=(0.5, 0.5))
            picture_io = io.BytesIO()
            picture.save(picture_io, format=ext)
            picture = ContentFile(picture_io.getvalue(), pic.name)
            return picture
        elif pic is not None and isinstance(pic, ImageFieldFile):
            raise forms.ValidationError(message='Произошла ошибка, попробуйте позже.')

    class Meta:
        fields = ('title', 'picture')

class CleanedPictureChangeForm(forms.ModelForm, CaseInsensitiveNameMixin):
    """Форма подготовки и валидации изображения в админке."""

    picture = forms.ImageField(label='Изображение',
        validators=[FileTypeValidator(allowed_types=['image/jpg', 'image/jpeg', 'image/png']),
        FileWeightValidator(weight=2*(1024*1024))], required=False)

    def clean_picture(self):
        pic_clear = self.data.get('picture-clear', None)

        if pic_clear:
            if isinstance(self.instance, Musician):
                self.instance.picture = '../static/img/default_musician.jpg'
                self.instance.save()
            elif isinstance(self.instance, Release):
                self.instance.picture = '../static/img/default_release.jpg'
                self.instance.save()

        pic = self.cleaned_data.get('picture', None)
        if pic is not None and isinstance(pic, UploadedFile):
            root, ext = pic.content_type.split('/')
            picture = Image.open(pic)
            picture = ImageOps.fit(picture, (500, 500), Image.ANTIALIAS, centering=(0.5, 0.5))
            picture_io = io.BytesIO()
            picture.save(picture_io, format=ext)
            picture = ContentFile(picture_io.getvalue(), pic.name)
            return picture
        elif pic is not None and isinstance(pic, ImageFieldFile):
            pass
