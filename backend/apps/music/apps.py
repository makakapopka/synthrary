from django.apps import AppConfig


class MusicConfig(AppConfig):
    name = 'apps.music'
    verbose_name = 'Музыка'
