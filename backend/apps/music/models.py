from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.contenttypes.fields import GenericRelation
from django.urls import reverse
from django.db.models.signals import pre_delete

from uuslug import slugify
from hitcount.models import HitCountMixin

from .managers import MusicManager
from ..utils.models import TimeStampMixin, IsActiveModelMixin
from ..utils.helpers import RandomFileNameHelper

class Genre(models.Model):
    """Модель жанра."""

    name = models.CharField(verbose_name='жанр', max_length=64, unique=True)
    slug = models.SlugField(verbose_name='слаг', max_length=64, unique=True, blank=True)

    NAME_FIELD = 'slug'

    objects = MusicManager()

    def slugify_name(self):
        self.slug = slugify(self.name)

    def save(self, *args, **kwargs):
        self.slugify_name()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'жанр'
        verbose_name_plural = 'жанры'

class Label(models.Model):
    """Модель лейбла."""

    name = models.CharField(verbose_name='лейбл', max_length=64, unique=True)
    slug = models.SlugField(verbose_name='слаг', max_length=64, unique=True, blank=True)

    NAME_FIELD = 'slug'

    objects = MusicManager()

    def slugify_name(self):
        self.slug = slugify(self.name)

    def save(self, *args, **kwargs):
        self.slugify_name()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'лейбл'
        verbose_name_plural = 'лейблы'

class Musician(TimeStampMixin, IsActiveModelMixin, HitCountMixin):
    """Модель музыканта."""

    # Основное
    name = models.CharField(
        verbose_name='название',
        blank=False, unique=True, max_length=64,
        error_messages={'unique': 'Музыкант с таким именем уже существует.',
                        }
        )

    NAME_FIELD = 'slug'
    objects = MusicManager()

    slug = models.SlugField(verbose_name='слаг', max_length=64, unique=True, blank=True)

    picture = models.ImageField(verbose_name='изображение', default='../static/img/default_musician.jpg',
                                upload_to=RandomFileNameHelper('musicians/'))
    irl_name = models.CharField(verbose_name='настоящее имя', blank=True, max_length=64)

    TYPES = (
        (None, 'Тип'),
        ('musician', 'Музыкант'),
        ('group', 'Группа')
    )
    type = models.CharField(verbose_name='тип', max_length=8, choices=TYPES, blank=True)
    members = models.CharField(verbose_name='участники', 
        blank=True, max_length=256, help_text='Если тип исполнителя - группа')

    GENDERS = (
        (None, 'Пол'),
        ('male', 'Мужской'),
        ('female', 'Женский')
    )
    gender = models.CharField(verbose_name='пол',
                              max_length=6, choices=GENDERS, blank=True)

    country = models.CharField(verbose_name='страна', blank=True, max_length=128)

    # Музыка
    genre = models.ManyToManyField(Genre, verbose_name='жанр', blank=True)
    other_projects = models.CharField(verbose_name='другие проекты', max_length=128, blank=True)

    # Сообщества
    vk_group = models.CharField(verbose_name='Сообщество в ВК', blank=True,
                         help_text='Логин сообщества',
                         max_length=64)

    def is_group(self):
        """Возращает ИСТИНУ, если есть хотя бы одна группа."""
        groups = [self.vk_group]
        for group in groups:
            if group:
                return True
                break

    def is_release(self):
        """Возращает ИСТИНУ, если есть хотя бы один релиз."""
        for release in self.releases.all():
            if release.is_active:
                return True
                break

    @property
    def releases_number(self):
        return self.releases.count()

    @property
    def activated_releases_number(self):
        return self.releases.activated.count()

    # Контакты
    vk = models.CharField(verbose_name='ВКонтакте', blank=True,
                         help_text='Логин в VK (логин или id12345678)',
                         max_length=64)
    facebook = models.CharField(verbose_name='Facebook', blank=True,
                               help_text='Логин в Facebook',
                               max_length=64)
    twitter = models.CharField(verbose_name='Twitter', blank=True,
                               help_text='Логин в Twitter', max_length=64)
    instagram = models.CharField(verbose_name='Instagram', blank=True,
                               help_text='Логин в Instagram', max_length=64)
    bandcamp = models.CharField(verbose_name='Bandcamp', blank=True,
                               help_text='Логин в Bandcamp', max_length=64)


    about = models.TextField(verbose_name='информация о музыканте',
                        max_length=30000, blank=True)

    def is_contact(self):
        """Возращает ИСТИНУ, если есть хотя бы один контакт."""
        contacts = [self.vk, self.facebook, self.twitter,
                    self.instagram]
        for contact in contacts:
            if contact:
                return True
                break

    def clean_name(self):
        """Проверка на уникальность имени в lowercase."""
        name = self.name
        if name is not None:
            if Musician.objects.filter(name__iexact=name).exclude(pk=self.pk).exists():
                raise ValidationError(message='Музыкант с таким именем уже существует.', code='unique')
        return name

    def slugify_name(self):
        self.slug = slugify(self.name)

    def get_absolute_url(self):
        return reverse('musician_detail', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        self.clean_name()
        self.slugify_name()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'музыкант'
        verbose_name_plural = 'музыканты'
        ordering = ['name', 'gender', 'country']


def delete_related_releases(sender, instance, **kwargs):
    """
    Удаляет все релизы музыканта, при его удалении.
    """

    for release in instance.releases.all():
        release.delete()

pre_delete.connect(delete_related_releases, sender=Musician)


class Release(TimeStampMixin, IsActiveModelMixin, HitCountMixin):
    """Модель релиза."""

    # Основное
    author = models.ManyToManyField(Musician, verbose_name='автор', related_name='releases')

    title = models.CharField(verbose_name='название', blank=False, max_length=64)

    picture = models.ImageField(verbose_name='изображение', default='../static/img/default_release.jpg',
                                upload_to=(RandomFileNameHelper('releases/')))

    date_released = models.DateField(verbose_name='дата релиза', blank=True, null=True)

    # Музыка
    genre = models.ManyToManyField(Genre, verbose_name='жанр', blank=True, related_name='genres')
    label = models.ForeignKey(Label, verbose_name='лейбл', blank=True, on_delete=models.SET_NULL, null=True)
    FORMATS = (
        (None, 'Формат'),
        ('ep', 'Мини-альбом'),
        ('lp', 'Лонг-плей'),
        ('cp', 'Компиляция'),
        ('sp', 'Сингл'),
    )
    format = models.CharField(verbose_name='формат', choices=FORMATS, max_length=12, blank=True)
    number_of_tracks = models.PositiveSmallIntegerField(verbose_name='количество треков', blank=True, null=True)
    tracklist = models.TextField(verbose_name='трек-лист', max_length=10000, blank=True)
    related_musicians = models.ManyToManyField(Musician, verbose_name='связанные музыканты', blank=True)

    about = models.TextField(verbose_name='информация о релизе',
                        max_length=30000, blank=True)

    # Ссылки
    support = models.URLField(verbose_name='поддержать', blank=True,
                         help_text='Ссылка на источники')
    listen = models.URLField(verbose_name='Слушать', blank=True,
                         help_text='Ссылка на плейлист ВК')

    comments = GenericRelation('comments.comment')

    @property
    def get_tracklist(self):
        if self.tracklist:
            return self.tracklist.split('\r\n')

    @property
    def get_author(self):
        if len(self.author.all()) == 1:
            return ''.join([str(a) for a in self.author.all()])
        else:
            return ' & '.join([str(a) for a in self.author.all()])

    @property
    def get_full_name(self):
        return f'{self.get_author} - {self.title}'

    @property  
    def is_link(self):
        """Возращает ИСТИНУ, если есть хотя бы одна ссылка."""
        links = [self.support, self.listen]
        for link in links:
            if link:
                return True
                break
    def get_absolute_url(self):
        return reverse('release_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'релиз'
        verbose_name_plural = 'релизы'
        ordering = ['-date_released']