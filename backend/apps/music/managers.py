from django.db import models

class MusicManager(models.Manager):
    use_in_migrations = True

    def create(self, **kwargs):
        """
        Create a new object with the given kwargs, saving it to the database
        and returning the created object.
        """
        obj = self.model(**kwargs)
        self._for_write = True
        obj.save(force_insert=True, using=self.db)
        return obj

    def get(self, **kwargs):
        if 'slug' in kwargs:
            kwargs['slug__iexact'] = kwargs['slug']
            del kwargs['slug']
        if 'name' in kwargs:
            kwargs['name__iexact'] = kwargs['name']
            del kwargs['name']
        return super(models.Manager, self).get(**kwargs)

    def get_by_name(self, slug):
        return self.get(slug__iexact=slug)

    def get_by_natural_key(self, slug):
        """
        By default, Django does a case-sensitive check on names. This is Wrong.
        Overriding this method fixes it.
        """
        return self.get(**{self.model.NAME_FIELD + '__iexact': slug})