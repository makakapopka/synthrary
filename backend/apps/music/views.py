from django.http import Http404
from django.views.generic import ListView

from hitcount.views import HitCountDetailView

from .models import Musician, Release
from .filters import MusiciansFilterSet, ReleasesFilterSet
from ..comments.views import DetailViewCommentsPaginatorMixin

"""
class PermissionWorkWithMusicMixin(LoginRequiredMixin, MultiplePermissionsRequiredMixin):
    redirect_unauthenticated_users = True
    raise_exception = True
    permissions = {
        "all": (
            'music.add_musician',
            'music.view_musician',
            'music.change_musician',
            'music.delete_musician',

            'music.add_release',
            'music.view_release',
            'music.change_release',
            'music.delete_release',

            'music.add_genre',
            'music.view_genre',
            'music.change_genre',
            'music.delete_genre',

            'music.add_label',
            'music.view_label',
            'music.change_label',
            'music.delete_label',
            ),
        }
"""

class MusicianDetailView(HitCountDetailView):
    """Страница музыканта."""

    model = Musician
    template_name = 'music/musician/musician.html'
    count_hit = True

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.is_active:
            raise Http404
        context = self.get_context_data(object=self.object, 
            releases=self.object.releases.exclude(is_active=False).order_by('-date_released') if self.object.releases else False)
        return self.render_to_response(context)

musician_detail_view = MusicianDetailView.as_view()

class ReleaseDetailView(DetailViewCommentsPaginatorMixin, HitCountDetailView):
    """Страница релиза."""

    model = Release
    template_name = 'music/release/release.html'
    count_hit = True

release_detail_view = ReleaseDetailView.as_view()

class MusiciansListView(ListView):
    """Страница музыкантов."""

    context_object_name = 'musicians'
    paginate_by = 16
    template_name = 'music/musician/musicians.html'
    extra_context = {'filter': MusiciansFilterSet}
    queryset = Musician.activated.all()

    def get_queryset(self):
        filtered_queryset = MusiciansFilterSet(self.request.GET, queryset=self.queryset).qs
        return filtered_queryset

musicians_list_view = MusiciansListView.as_view()

class ReleasesListView(ListView):
    """Страница релизов."""

    context_object_name = 'releases'
    paginate_by = 16
    template_name = 'music/release/releases.html'
    extra_context = {'filter': ReleasesFilterSet}
    queryset = Release.activated.all()

    def get_queryset(self):
        filtered_queryset = ReleasesFilterSet(self.request.GET, queryset=self.queryset).qs
        return filtered_queryset

releases_list_view = ReleasesListView.as_view()