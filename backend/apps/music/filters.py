import django_filters

from .models import Musician, Genre, Release, Label

BOOLEAN_CHOICES = (('false', 'Нет'), ('true', 'Да'),)

class MusiciansFilterSet(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains', label='По имени')
    genre = django_filters.ModelChoiceFilter(
        queryset=Genre.objects.order_by('name'), label='По жанру',
        empty_label='Все жанры',
        )

    TYPES = (
    (None, 'Все типы'),
    ('musician', 'Музыкант'),
    ('group', 'Группа')
    )

    type = django_filters.ChoiceFilter(choices=TYPES, label='По типу', empty_label='Все типы')

    class Meta:
        model = Musician
        fields = ['name', 'genre', 'type']

class ReleasesFilterSet(django_filters.FilterSet):
    title = django_filters.CharFilter(lookup_expr='icontains')

    author = django_filters.CharFilter(lookup_expr='iexact', 
        field_name='author__name')

    genre = django_filters.ModelChoiceFilter(
        queryset=Genre.objects.order_by('name'),
        empty_label='Все жанры')

    label = django_filters.ModelChoiceFilter(
        queryset=Label.objects.order_by('name'),
        empty_label='Все лейблы')

    FORMATS = (
        (None, 'Все форматы'),
        ('ep', 'Мини-альбом'),
        ('lp', 'Лонг-плей'),
        ('cp', 'Компиляция'),
        ('sp', 'Сингл'),
    )

    format = django_filters.ChoiceFilter(choices=FORMATS, 
        empty_label='Все форматы')

    ordering = django_filters.OrderingFilter(
        fields=(('date_released', '-date_released'), ('date_released', 'date_released'))
    )

    class Meta:
        model = Release
        fields = ['title', 'author', 'genre', 'label', 'format', 'ordering']