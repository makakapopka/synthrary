from django.urls import path

from . import views


urlpatterns = [

    # Новости
    path('', views.news_list_view, name='news'),
    path('<int:pk>/', views.new_detail_view, name='new_detail'),
]