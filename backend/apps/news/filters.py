from .models import New
from ..contents.filters import ContentFilterSet

class NewsFilterSet(ContentFilterSet):
    """Фильтр новостей."""

    class Meta:
        model = New
        fields = ['title', 'category', 'tags', 'ordering']