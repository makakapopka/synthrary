"""
from braces.views import LoginRequiredMixin, MultiplePermissionsRequiredMixin


class PermissionWorkWithNewsMixin(LoginRequiredMixin, MultiplePermissionsRequiredMixin):
    redirect_unauthenticated_users = True
    raise_exception = True
    permissions = {
        "all": (
            'news.add_new',
            'news.view_new',
            'news.change_new',
            'news.delete_new',

            'news.add_category',
            'news.view_category',
            'news.change_category',
            'news.delete_category',

            'news.add_tag',
            'news.view_tag',
            'news.change_tag',
            'news.delete_tag',
            ),
        }
"""