from django.contrib import admin
from django.urls import reverse

from .models import New
from ..contents.admin import ContentAdminMixin


@admin.register(New)
class NewAdmin(ContentAdminMixin, admin.ModelAdmin):
    pass