from django.db import models
from django.urls import reverse

from ..contents.models import BaseContentModelMixin
from ..utils.helpers import RandomFileNameHelper


class New(BaseContentModelMixin):
    """Модель новости."""

    picture = models.ImageField(verbose_name='изображение', upload_to=RandomFileNameHelper('news/'),
                                max_length=256, blank=True, null=True)

    def get_absolute_url(self):
        return reverse('new_detail', kwargs={'pk': self.pk})

    class Meta:
        verbose_name = 'новость'
        verbose_name_plural = 'новости'
        ordering = ['-published_at']