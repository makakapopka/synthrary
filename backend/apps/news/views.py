from django.views.generic import ListView

from hitcount.views import HitCountDetailView

from .models import New
from .filters import NewsFilterSet
from ..comments.views import DetailViewCommentsPaginatorMixin

class NewsListView(ListView):
    """Страница со списком новостей."""

    context_object_name = 'news'
    paginate_by = 10
    template_name = 'news/news.html'
    extra_context = {'filter': NewsFilterSet}
    queryset = New.activated.all()

    def get_queryset(self):
        filtered_queryset = NewsFilterSet(self.request.GET, queryset=self.queryset).qs
        return filtered_queryset

news_list_view = NewsListView.as_view()

class NewDetailView(DetailViewCommentsPaginatorMixin, HitCountDetailView):
    """Страница новости."""

    model = New
    template_name = 'news/new.html'
    count_hit = True

new_detail_view = NewDetailView.as_view()