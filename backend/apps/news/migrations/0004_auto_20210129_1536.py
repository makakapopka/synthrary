# Generated by Django 3.1.3 on 2021-01-29 10:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0003_new_published_at'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='new',
            options={'ordering': ['-published_at'], 'verbose_name': 'новость', 'verbose_name_plural': 'новости'},
        ),
    ]
