from django.contrib import admin

from .models import Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug')
    list_display_links = ('name', 'slug')
    ordering = ('name', 'slug')
    sortable_by = ('name', 'slug')
    search_fields = ('name', 'slug')
    list_filter = ('name', 'slug')
    readonly_fields = ('slug', )