from django.db import models

from uuslug import slugify


class Category(models.Model):
    """Модель категории."""

    name = models.CharField(verbose_name='категория', max_length=64, unique=True)
    slug = models.SlugField(verbose_name='слаг', max_length=64, unique=True, blank=True)

    def slugify_name(self):
        self.slug = slugify(self.name)

    def save(self, *args, **kwargs):
        self.slugify_name()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'категория'
        verbose_name_plural = 'категории'