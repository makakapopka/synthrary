# Generated by Django 3.1.3 on 2020-11-20 05:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='donated_sum',
            field=models.SmallIntegerField(default=0, verbose_name='общая сумма пожертвований'),
        ),
    ]
