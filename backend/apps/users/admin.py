from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.urls import reverse

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import User, UserProfile


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    list_display = ('username', 'email', 'is_active')
    list_display_links = ('username', 'email')
    list_editable = ('is_active', )
    ordering = ('username', 'email', 'is_active')
    sortable_by = ('username', 'email', 'is_active')
    search_fields = ('username', 'email')
    list_filter = ('is_active', 'send_messages', 'is_superuser', 'is_staff', 'groups')
    fieldsets = (
        ('Общее', {'fields': ('username', 'email','password', 'donated_sum')}),

        ('Привилегии', {'fields': ('is_active', 'send_messages', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')
                         }
         ),
    )

@admin.register(UserProfile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', )
    list_display_links = ('user', )
    ordering = ('user', )
    sortable_by = ('user', )
    search_fields = ('user', 'business', 'hobby', 'location')
    list_filter = ('user', )
    raw_id_fields = ('user', )
    fieldsets = (
        ('Общее', {
            'fields': ('user', 'picture', 'full_name', 'gender',
                        'birthday', 'business', 'hobby', 'location')
            }
         ),

        ('Музыка', {
            'fields': ('favourite_synthwave_style', 'favourite_synthwave_musician',
                        'favourite_another_style', 'favourite_another_musician')
        }),

        ('Контакты', {
            'fields': ('vk', 'facebook', 'twitter', 'instagram',
                        'discord', 'skype', 'telegram', 'email')
        }),

        ('О себе', {
            'fields': ('about', )
        })
    )

    def view_on_site(self, obj):
        return reverse('profile', kwargs={'slug': obj.user.username})