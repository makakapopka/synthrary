from django import template


register = template.Library()

@register.simple_tag(takes_context=True)
def is_current_view(context, viewname):
    """Возвращает True, если 'viewname' соответствует запросу."""

    if context['request'].resolver_match.url_name == viewname:
        return True
    else:
        return False