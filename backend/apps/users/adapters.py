from allauth.account.adapter import DefaultAccountAdapter

from .models import User


class CustomDefaultAccountAdapter(DefaultAccountAdapter):
    error_messages = {
        'username_blacklisted': 'Использовать это имя запрещено',
        'username_taken': User._meta.get_field('username').error_messages['unique'],
        'too_many_login_attempts':'Слишком много попыток входа. Попробуйте позднее',
        'email_taken': User._meta.get_field('email').error_messages['unique'],
    }