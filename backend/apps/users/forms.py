import io

from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import UploadedFile
from django.db.models.fields.files import ImageFieldFile

from PIL import Image, ImageOps
from allauth.account.forms import SignupForm, LoginForm, ChangePasswordForm, PasswordField, ResetPasswordForm
from allauth.account.adapter import get_adapter
from allauth.account.utils import filter_users_by_email
from allauth.account.models import EmailAddress

from .models import User, UserProfile
from ..utils.validators import FileTypeValidator, FileWeightValidator
from ..utils.forms import CaptchaField


class CaseInsensitiveUsernameMixin():
    """
    Disallow a username with a case-insensitive match of existing usernames.
    Add this mixin to any forms that use the User object
    """

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if User.objects.filter(username__iexact=username) \
                                   .exclude(pk=self.instance.pk).exists():
            raise forms.ValidationError('Имя пользователя ‘{}’ уже занято.'.format(username))
        return username

class CustomSignupForm(SignupForm):
    """Форма регистрации, наследуемая от allauth."""

    captcha = CaptchaField

    field_order = [
        'username',
        'email',
        'password1',
        'password2',
        'captcha'
    ]

class CustomLoginForm(LoginForm):
    """Форма логина, наследуемая от allauth."""
    
    captcha = CaptchaField

class CustomPasswordResetForm(ResetPasswordForm):
    """Форма сброса пароля, наследуемая от allauth."""

    captcha = CaptchaField

class CustomUserChangeForm(CaseInsensitiveUsernameMixin, UserChangeForm):
    """Форма изменения пользователя в админке."""

    pass

class CustomUserCreationForm(CaseInsensitiveUsernameMixin, UserCreationForm):
    """Форма создания пользователя в админке."""

    pass


class EditProfilePictureForm(forms.ModelForm):
    """Форма изменения аватарки пользователя."""

    picture = forms.ImageField(
        label='',
        widget=forms.FileInput(attrs={'onchange':'readURL(this);'}),
        validators=[
            FileTypeValidator(allowed_types=['image/jpg', 'image/jpeg', 'image/png']),
            FileWeightValidator(weight=2*(1024*1024))]
    )

    def clean_picture(self):
        pic = self.cleaned_data.get('picture', None)
        if pic is not None and isinstance(pic, UploadedFile):
            root, ext = pic.content_type.split('/')
            picture = Image.open(pic)
            picture = ImageOps.fit(picture, (500, 500), Image.ANTIALIAS, centering=(0.5, 0.5))
            picture_io = io.BytesIO()
            picture.save(picture_io, format=ext)
            picture = ContentFile(picture_io.getvalue(), pic.name)
            return picture
        elif pic is not None and isinstance(pic, ImageFieldFile):
            raise forms.ValidationError(message='Произошла ошибка, попробуйте позже.')

    class Meta:
        model = UserProfile
        fields = ('picture', )


class ChangeEmailForm(forms.Form):
    """Форма смены почты."""

    password = PasswordField()
    email = forms.EmailField(required=True)
    confirm_email = forms.EmailField(required=True)
    captcha = CaptchaField

    def __init__(self, user=None, *args, **kwargs):
        self.user = user
        super(ChangeEmailForm, self).__init__(*args, **kwargs)

    def clean_password(self):
        if not self.user.check_password(self.cleaned_data.get('password')):
            raise forms.ValidationError('Пожалуйста, введите ваш текущий пароль.')
        return self.cleaned_data['password']

    def clean_email(self):
        value = self.cleaned_data.get('email')
        value = get_adapter().clean_email(value)
        errors = {
            "this_account": "Данный адрес электронной почты уже привязан к этому аккаунту.",
            "different_account": "Данный адрес электронной почты уже занят."
        }
        users = filter_users_by_email(value)
        on_this_account = [u for u in users if u.pk == self.user.pk]
        on_diff_account = [u for u in users if u.pk != self.user.pk]

        if on_this_account:
            raise forms.ValidationError(errors["this_account"])
        if on_diff_account:
            raise forms.ValidationError(errors["different_account"])
        return value


    def clean(self):
        cleaned_data = super(ChangeEmailForm, self).clean()
        email = self.cleaned_data.get('email')
        confirm_email = self.cleaned_data.get('confirm_email')
        if (email and confirm_email) and email != confirm_email:
            self.add_error(
                'email_mismatch', "Вы должны ввести один и тот же адрес электронной почты дважды."
            )
        return cleaned_data

    def save(self, request):
        email = self.cleaned_data.get('email')
        user = request.user

        # удалить прошлые адреса
        try:
            non_primary_emails = EmailAddress.objects.filter(user=user, primary=False)
            for email_instance in non_primary_emails:
                email_instance.delete()
        except EmailAddress.DoesNotExist:
            pass

        return EmailAddress.objects.add_email(request, user, email, confirm=True)


class CustomChangePasswordForm(ChangePasswordForm):
    """Форма смены пароля."""

    captcha = CaptchaField