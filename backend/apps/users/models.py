from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.core.exceptions import ValidationError
from django.urls import reverse

from .managers import UserManager
from . import validators
from ..utils.helpers import RandomFileNameHelper


class User(AbstractBaseUser, PermissionsMixin):
    """
    Собственная модель пользователя, использующая технологию запрета регистрации
    пользователей с одинаковыми в lowercase именами.
    """
    username = models.CharField(
        verbose_name='имя пользователя',
        max_length=32, unique=True, validators=[validators.validate_username],
        error_messages={'unique': 'Пользователь с таким именем уже существует.',
                        }
    )

    email = models.EmailField(
        verbose_name='электронная почта',
        unique=True, validators=[validators.email_validator],
        error_messages={'unique': 'Пользователь с такой электронной почтой уже существует.'
                        }
    )

    date_joined = models.DateTimeField(verbose_name='дата регистрации', auto_now_add=True)

    is_active = models.BooleanField(verbose_name='активен', default=True)
    is_staff = models.BooleanField(
        verbose_name='статус персонала', default=False,
        help_text='Определяет может ли пользователь входить в административную часть сайта'
    )
    send_messages = models.BooleanField(verbose_name='слать оповещения о новых записях?', default=True)

    donated_sum = models.SmallIntegerField(verbose_name='общая сумма пожертвований', default=0)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['email']

    def clean_username(self):
        """
        Проверка на уникальность имени в lowercase.
        """
        username = self.username
        if username is not None:
            validators.validate_username(username)
            if User.objects.filter(username__iexact=username).exclude(pk=self.pk).exists():
                raise ValidationError(message='Имя ‘{}’ уже занято!.'.format(username), code='unique')
        return username

    def save(self, *args, **kwargs):
        self.clean_username()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

class UserProfile(models.Model):
    """
    Модель профиля пользователя.
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, verbose_name='пользователь',
        related_name='profile')

    picture = models.ImageField(verbose_name='аватар', default='avatars/default_avatar.jpg',
                                upload_to=RandomFileNameHelper('avatars/'))

    full_name = models.CharField(verbose_name='полное имя', max_length=64, blank=True)

    # Основная информация
    GENDERS = (
        (None, 'Пол'),
        ('male', 'Мужской'),
        ('female', 'Женский')
    )
    gender = models.CharField(verbose_name='пол',
                              max_length=6, choices=GENDERS, blank=True)

    birthday = models.DateField(verbose_name='день рождения', null=True, blank=True,
                                help_text='В формате дд.мм.гггг')

    # Интересы
    business = models.CharField(
        verbose_name='род занятий', max_length=128,
        blank=True)
    hobby = models.CharField(
        verbose_name='интересы', max_length=128,
        blank=True)

    # Место жительства
    location = models.CharField(verbose_name='место жительства', blank=True, max_length=128)

    # Любимая музыка
    favourite_synthwave_style = models.CharField(verbose_name='любимый стиль', 
        blank=True, max_length=64)
    favourite_synthwave_musician = models.CharField(verbose_name='любимый музыкант', 
        blank=True, max_length=64)
    favourite_another_style = models.CharField(verbose_name='другой жанр', blank=True, max_length=64)
    favourite_another_musician = models.CharField(verbose_name='любимый музыкант', blank=True,
                                                  max_length=64)

    about = models.TextField(verbose_name='о себе', max_length=5000, blank=True)

    # Контакты
    vk = models.CharField(verbose_name='ВКонтакте', blank=True,
                         help_text='Логин в VK (логин или id12345678)',
                         max_length=64)
    facebook = models.CharField(verbose_name='Facebook', blank=True,
                               help_text='Логин в Facebook',
                               max_length=64)
    twitter = models.CharField(verbose_name='Twitter', blank=True,
                               help_text='Логин в Twitter', max_length=64)
    instagram = models.CharField(verbose_name='Instagram', blank=True,
                               help_text='Логин в Instagram', max_length=64)
    discord = models.CharField(verbose_name='Discord', blank=True,
                               help_text='Логин#ID', max_length=64)
    skype = models.CharField(verbose_name='Skype',
                             help_text='Логин в Skype', blank=True, max_length=64)
    telegram = models.CharField(
        verbose_name='Telegram', blank=True,
        help_text='Логин в Telegram', max_length=64)
    email = models.EmailField(verbose_name='Email', blank=True)

    def calculate_age(self):
        """
        Вычисляет возраст.
        """
        import datetime
        return int((datetime.date.today() - self.birthday).days / 365.25)

    # Возраст
    age = property(calculate_age)

    @property
    def is_common_info(self):
        """
        Возращает ИСТИНУ, если есть хотя бы одна ифнормация.
        Используется в шаблоне.
        """
        common_info = [self.full_name, self.gender, self.birthday, self.business, self.hobby]
        for info in common_info:
            if info:
                return True
                break

    @property
    def is_contact(self):
        """
        Возращает ИСТИНУ, если есть хотя бы один контакт.
        Используется в шаблоне.
        """
        contacts = [self.vk, self.facebook, self.twitter,
                    self.instagram, self.discord, self.skype,
                    self.telegram, self.email]
        for contact in contacts:
            if contact:
                return True
                break

    @property
    def is_synthwave(self):
        """
        Возращает ИСТИНУ, если есть хотя бы один стиль.
        Используется в шаблоне.
        """
        synthwave_choices = [self.favourite_synthwave_style, 
            self.favourite_synthwave_musician]
        for choice in synthwave_choices:
            if choice:
                return True
                break

    @property
    def is_another_style(self):
        """
        Возращает ИСТИНУ, если есть хотя бы один стиль.
        Используется в шаблоне.
        """
        another_choices = [self.favourite_another_style, 
            self.favourite_another_musician]
        for choice in another_choices:
            if choice:
                return True
                break

    @staticmethod
    def autocomplete_search_fields():
        return 'user'

    def __str__(self):
        return self.user.username

    def get_absolute_url(self):
        return reverse('profile', kwargs={'username': self.user.username})

    class Meta:
        verbose_name = 'профиль'
        verbose_name_plural = 'профили'

