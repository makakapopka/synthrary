from django.core.exceptions import ValidationError
from django.contrib.auth.validators import validators

from ..reserved_names import DEFAULT_RESERVED_NAMES


# Проверка на присутсвие недопустимых символов в нике и его длины
username_chars_validator = validators.RegexValidator(
    regex='^[a-zA-Z][a-zA-Z0-9_]{2,32}$',
    message='Только буквы (A-Z a-z) и цифры (0-9), не меньше 2 и не больше 32 символов.'
)

# Проверка на запрещенные имена
def validate_username(value):
    """Получает username и проверяет, чтобы его небыло в черном списке"""
    if str(value).lower() in DEFAULT_RESERVED_NAMES:
        raise ValidationError('Данное имя запрещено использовать.', code='blacklist_name')
    username_chars_validator(value)
    return value

# Проверка почты
email_validator = validators.EmailValidator(message='Введите корректный адресс электронной почты.')

# Проверка на присутсвие недопустимых символов в имени и фамилии и его длины
name_validator = validators.RegexValidator(
    regex='[A-ZА-Я][A-zА-я]{2,64}$',
    message='Только буквы (A-Z a-z) и (А-Я а-я), не меньше 2 и не больше 128 символов, начало с заглавной буквы.')
