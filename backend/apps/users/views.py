from django.views.generic import UpdateView, DetailView, FormView
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404, reverse
from django.http import HttpResponseRedirect
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.shortcuts import redirect

from braces.views import LoginRequiredMixin

from allauth.account.views import LogoutView, PasswordChangeView, ConfirmEmailView
from allauth.account.adapter import get_adapter
from allauth.account import signals
from allauth.account.models import EmailAddress
from allauth.account.utils import sync_user_email_addresses

from .models import User, UserProfile
from . import forms
from ..utils.forms import CaptchaForm


class GetUserProfileMixin(LoginRequiredMixin):
    """Миксин для получения профиля авторизованного пользователя."""

    model = UserProfile
    context_object_name = 'profile'

    def get_object(self):
        return get_object_or_404(self.model, user=self.request.user)


class UserProfileView(DetailView):
    """Страница профиля пользователя."""

    model = UserProfile
    template_name = 'account/profile.html'
    context_object_name = 'profile'
    slug_field = 'username'

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        slug = self.kwargs.get(self.slug_url_kwarg, None)
        user = get_object_or_404(User, username__iexact=slug)
        obj = get_object_or_404(queryset, user=user)
        return obj

profile_view = UserProfileView.as_view()

class EditProfileView(GetUserProfileMixin, DetailView):
    """Страница выбора редактирования профиля."""

    template_name = 'account/edit_profile.html'

edit_profile_view = EditProfileView.as_view()


class EditProfilePictureView(SuccessMessageMixin, GetUserProfileMixin, UpdateView):
    """Страница редактирования аватара профиля."""

    form_class = forms.EditProfilePictureForm
    template_name = 'account/edit_profile.html'
    success_url = reverse_lazy('edit_profile')
    success_message = 'Изменение аватара было успешно выполнено!'

edit_profile_picture_view = EditProfilePictureView.as_view()


class EditProfileBaseView(SuccessMessageMixin, GetUserProfileMixin, UpdateView):
    """Страница редактирования основной информации профиля."""

    fields = ('full_name', 'location', 'gender', 'birthday', 'business',
                  'hobby', 'favourite_synthwave_style',
                  'favourite_synthwave_musician', 'favourite_another_style',
                  'favourite_another_musician', 'about')
    template_name = 'account/edit_profile.html'
    success_url = reverse_lazy('edit_profile_base')
    success_message = 'Изменение основной информации было успешно выполнено!'

edit_profile_base_view = EditProfileBaseView.as_view()


class EditProfileContacts(SuccessMessageMixin, GetUserProfileMixin, UpdateView):
    """Страница редактирования контактов профиля."""

    fields = ('vk', 'facebook', 'twitter', 'instagram', 'discord', 'skype', 'telegram', 'email')
    template_name = 'account/edit_profile.html'
    success_url = reverse_lazy('edit_profile_contacts')
    success_message = 'Изменение контактов было успешно выполнено!'

edit_profile_contacts_view = EditProfileContacts.as_view()


class EditUserSettigns(GetUserProfileMixin, DetailView):
    """Добавление капчи для редактирования настроек пользователя."""

    template_name = 'account/account_settings.html'
    extra_context = {
        'captcha': CaptchaForm
    }

edit_user_settings_view = EditUserSettigns.as_view()


class CustomPasswordChangeView(GetUserProfileMixin, PasswordChangeView):
    """Страница смены пароля."""

    template_name = 'account/account_settings.html'
    success_url = reverse_lazy('account_settings')
    form_class = forms.CustomChangePasswordForm

    def render_to_response(self, context, **response_kwargs):
        if not self.request.user.has_usable_password():
            return HttpResponseRedirect(reverse('account_settings'))
        return super(PasswordChangeView, self).render_to_response(
            context, **response_kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context[self.context_object_name] = self.get_object()
        return context

password_change_view = CustomPasswordChangeView.as_view()


class EmailChangeView(GetUserProfileMixin, FormView):
    """Страница смены почты."""

    success_url = reverse_lazy('account_settings')
    form_class = forms.ChangeEmailForm
    template_name = 'account/account_settings.html'

    def dispatch(self, request, *args, **kwargs):
        sync_user_email_addresses(request.user)
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def form_valid(self, form):
        email_address = form.save(self.request)

        get_adapter(self.request).add_message(
            self.request,
            messages.INFO,
            'account/messages/'
            'email_confirmation_sent.txt',
            {'email': form.cleaned_data["email"]})
        signals.email_added.send(sender=self.request.user.__class__,
                                 request=self.request,
                                 user=self.request.user,
                                 email_address=email_address)

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['email_change_form'] = context.get('form')
        context[self.context_object_name] = self.get_object()
        return context


email_change_view = EmailChangeView.as_view()


class CustomConfirmEmailView(ConfirmEmailView):
    """Страница подтверждения Email. Удаляет старые email-адреса."""

    def post(self, *args, **kwargs):
        self.object = confirmation = self.get_object()

        user = confirmation.email_address.user
        email = confirmation.email_address.email

        try:
            email_instance = EmailAddress.objects.get_primary(user=user)
            email_instance.delete()
        except EmailAddress.DoesNotExist:
            pass

        User.objects.set_email(user, email)

        confirmation.confirm(self.request)


        get_adapter(self.request).add_message(
            self.request,
            messages.SUCCESS,
            'account/messages/email_confirmed.txt',
            {'email': confirmation.email_address.email})
        redirect_url = self.get_redirect_url()

        confirmation.delete()

        if not redirect_url:
            ctx = self.get_context_data()
            return self.render_to_response(ctx)
        return redirect(redirect_url)

confirm_email = CustomConfirmEmailView.as_view()


class CustomLogoutView(LogoutView, LoginRequiredMixin):
    """Выход из аккаунта."""

    template_name = 'account/logout.html'

logout = CustomLogoutView.as_view()