from django.urls import path, re_path
from allauth.account import views as allauth_views

from . import views

# allauth маршруты
allauth_urlpatterns = [

    # авторизация
    path("accounts/signup/", allauth_views.signup, name="account_signup"),
    path("accounts/login/", allauth_views.login, name="account_login"),
    path("accounts/logout/", views.logout, name="account_logout"),
    path("accounts/inactive/", allauth_views.account_inactive, name="account_inactive"),

    re_path(r"^accounts/password/change/$", views.password_change_view,
        name="account_change_password"),

    re_path(r"^accounts/email/$", views.email_change_view, name="account_change_email"),
    re_path(r"^accounts/confirm-email/$", allauth_views.email_verification_sent,
        name="account_email_verification_sent"),
    re_path(r"^accounts/confirm-email/(?P<key>[-:\w]+)/$", views.confirm_email,
        name="account_confirm_email"),

    # сброс пароля
    re_path(r"^accounts/password/reset/$", allauth_views.password_reset,
        name="account_reset_password"),
    re_path(r"^accounts/password/reset/done/$", allauth_views.password_reset_done,
        name="account_reset_password_done"),
    re_path(r"^accounts/password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$",
        allauth_views.password_reset_from_key,
        name="account_reset_password_from_key"),
    re_path(r"^accounts/password/reset/key/done/$", allauth_views.password_reset_from_key_done,
        name="account_reset_password_from_key_done"),

]

# Мои маршруты
urlpatterns = [
    # изменение профиля
    path('users/profile/edit/', views.edit_profile_view, name='edit_profile'),
    path('users/profile/edit/base/', views.edit_profile_base_view, name='edit_profile_base'),
    path('users/profile/edit/contacts/', views.edit_profile_contacts_view, name='edit_profile_contacts'),
    path('users/profile/edit/picture/', views.edit_profile_picture_view, name='edit_profile_picture'),

    path('accounts/settings/', views.edit_user_settings_view, name='account_settings'),

    # профиль
    path('users/profile/<slug:slug>/', views.profile_view, name='profile')

]

urlpatterns = allauth_urlpatterns + urlpatterns