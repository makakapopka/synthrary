from django.db.models.signals import post_save

from .models import User, UserProfile


def create_user_profile(sender, instance, created, **kwargs):
    """
    Создает профайл пользователя при создании юзера.
    """
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)