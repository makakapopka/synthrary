from django.db import models
from django.db.models import Sum

from ..users.models import User


class DonationsManager(models.Manager):
    """Менеджер для модели донатов."""

    def get_donators(self):
    	"""Возвращает QuerySet донатеров, которые прошли регистрацию."""

    	donations = super().get_queryset().exclude(author=None).distinct('author')
    	return User.objects.filter(donations__in=donations).order_by('-donated_sum')

    def get_donated_sum_from_donater(self, donator):
    	"""Возвращает общее количество денег, задоначеное пользователем."""

    	return donator.donations.aggregate(Sum('amount')).get('amount__sum', 0)
