from django.db import models
from django.utils.timezone import make_naive

from ..utils.models import TimeStampMixin
from ..users.models import User
from ..users.validators import email_validator
from .managers import DonationsManager


class Donation(TimeStampMixin):
	author = models.ForeignKey(
		User, verbose_name='автор',
		on_delete=models.SET_NULL, blank=True, null=True, related_name='donations')

	amount = models.SmallIntegerField(verbose_name='сумма')
	message = models.CharField(verbose_name='пожелание', max_length=512)

	objects = DonationsManager()

	class Meta:
		verbose_name = "Пожертвование"
		verbose_name_plural = "Пожертвования"

	def __str__(self):
		if not self.author:
			author = 'Неизвестный пользователь'
		else:
			author = self.author
		return f"{author} пожертвовал {self.amount}₽ ({make_naive(self.created_at).strftime('%d.%m.%Y, в %H:%M')})"
