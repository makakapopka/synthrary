import requests
from hashlib import sha1

from django.test import TestCase
from django.urls import reverse_lazy
from django.conf import settings


def _get_validation_hash_string():
	"""Возвращает sha1_hash строку для валидации платежа."""

	hash_string = "p2p-incoming&" \
				  "12345&" \
				  "545&" \
				  "643&" \
				  "2014-04-28T16:31:28Z&" \
				  "41003188981230&" \
				  "false&" \
				  f"{settings.YANDEX_MONEY_SECRET_KEY}&" \
				  "1".encode("UTF-8")

	return sha1(hash_string).hexdigest()

class YandexDonationTests(TestCase):

	data = {
		'notification_type': 'p2p-incoming',
		'operation_id': '12345',
		'amount': '545',
		'currency': '643',
		'datetime': '2014-04-28T16:31:28Z',
		'sender': '41003188981230',
		'codepro': 'false',
		'unaccepted': 'false',
		'label': '1',
		'sha1_hash': _get_validation_hash_string()
	}

	def test_external_empty_post_request(self):
		response = self.client.post(reverse_lazy('got_donation_from_yandex'))
		self.assertEqual(response.status_code, 403)

	def test_external_full_post_request_with_sha1(self):
		response = self.client.post(
			reverse_lazy('got_donation_from_yandex'),
			data=self.data
		)
		self.assertEqual(response.status_code, 200)

def test_request():

	data = {
		'notification_type': 'p2p-incoming',
		'operation_id': '12345',
		'amount': '545',
		'currency': '643',
		'datetime': '2014-04-28T16:31:28Z',
		'sender': '41003188981230',
		'codepro': 'false',
		'unaccepted': 'false',
		'label': '1',
		'sha1_hash': _get_validation_hash_string()
	}

	requests.post("http://localhost:8000/donations/api/yandex/", data=data)