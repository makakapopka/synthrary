from django.urls import path

from . import views


urlpatterns = [
	path('', views.donations, name='donations'),
	path('api/yandex/', views.got_donation_from_yandex_view, name='got_donation_from_yandex')
]