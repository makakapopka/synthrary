from django.contrib import admin

from .models import Donation


@admin.register(Donation)
class DonationsAdmin(admin.ModelAdmin):
    list_display = ('id', '__str__', 'created_at', 'author', 'amount', 'truncated_message')
    list_display_links = ('__str__', )
    ordering = ('-created_at', )
    sortable_by = ('created_at', 'author', 'amount')
    search_fields = ('author', 'amount', 'message')
    list_filter = ('created_at', 'amount', 'author')

    def truncated_message(self, obj):
        return obj.message[:120] + '...'
    truncated_message.short_description = 'Пожелание'