import os
from hashlib import sha1

from distutils.util import strtobool

from django.shortcuts import get_object_or_404
from django.core.exceptions import PermissionDenied
from django.conf import settings

from .models import Donation
from ..users.models import User


class YandexMoneyAPI:
	"""Класс, работающий с донатами от Yandex Money (ЮMoney)."""

	def __init__(self, request):
		"""Конструктор принимает request."""

		self.data = request.POST
		self.cleaned_data = {}

	def _validate_donation_integrity(self):
		"""Проверяет целостность тела POST уведомления."""

		try:
			self.data['notification_type']
			self.data['operation_id']
			self.data['amount']
			self.data['currency']
			self.data['datetime']
			self.data['sender']
			self.data['codepro']
			self.data['unaccepted']
			self.data['label']
			self.data['sha1_hash']
		except KeyError:
			raise PermissionDenied


	def _get_validation_hash_string(self):
		"""Возвращает sha1_hash строку для валидации платежа."""

		hash_string = f"{self.data['notification_type']}&" \
					  f"{self.data['operation_id']}&" \
					  f"{self.data['amount']}&" \
					  f"{self.data['currency']}&" \
					  f"{self.data['datetime']}&" \
					  f"{self.data['sender']}&" \
					  f"{self.data['codepro']}&" \
					  f"{settings.YANDEX_MONEY_SECRET_KEY}&" \
					  f"{self.data['label']}".encode("UTF-8")

		return sha1(hash_string).hexdigest()


	def _validate_donation_hash(self):
		"""Проводит валидацию путем сравнения получившегося хэша."""

		my_hash = self._get_validation_hash_string()
		yandex_hash = self.data['sha1_hash']

		if yandex_hash != my_hash:
			raise PermissionDenied

	def _validate_donation_accessibility(self):
		"""Проверяет донат на зачисление."""

		unaccepted = bool(strtobool(self.data['unaccepted']))
		codepro = bool(strtobool(self.data['codepro']))

		if unaccepted or codepro:
			raise PermissionDenied

	def _validate_donation(self):
		"""Выполняет полную валидацию доната."""

		self._validate_donation_integrity()
		self._validate_donation_hash()
		self._validate_donation_accessibility()


	def _clean_donation_data(self):
		"""Метод возвращает необходимые для работы данные в словаре,
			предварительно сделав очистку."""

		author_id = self.data.get('label', None)
		if author_id:
			try:
				author = get_object_or_404(User, pk=int(author_id))
			except ValueError:
				raise PermissionDenied
		else:
			author = None

		amount = int(float(self.data['amount']))

		self.cleaned_data = {
			'author': author,
			'amount': amount,
		}

	def _add_donation_to_db(self):
		"""Добавляет донат в базу данных."""

		return Donation.objects.create(**self.cleaned_data)

	def process_donation(self):
		"""Выполняет полную обработку доната, 
			возращает его созданный экземпляр."""
		self._validate_donation()
		self._clean_donation_data()
		return self._add_donation_to_db()