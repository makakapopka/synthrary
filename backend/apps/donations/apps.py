from django.apps import AppConfig


class DonationsConfig(AppConfig):
    name = 'apps.donations'
    verbose_name = 'Пожертвования'

    def ready(self):
    	from . import signals