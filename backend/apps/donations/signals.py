from django.db.models import Sum
from django.db.models.signals import post_save

from ..donations.models import Donation


def update_donated_sum(sender, instance, created, **kwargs):
    """Обновляет общее количество денег, задоначеное пользователем."""

    if created:
    	if instance.author:
	        instance.author.donated_sum = instance.author.donations.aggregate(Sum('amount')).get('amount__sum', 0)
	        instance.author.save()

post_save.connect(update_donated_sum, sender=Donation)