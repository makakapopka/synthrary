from django.views.generic import View, TemplateView
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import mail_managers
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import PermissionDenied

from .models import Donation
from . import services


class DonationsView(TemplateView):
    """Страница с формой отправки доната, донатерами и донатами."""

    template_name = 'pages/donations.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['donators'] = Donation.objects.get_donators()[:10]
        context['donations'] = Donation.objects.order_by('-created_at')[:3]
        return context

donations = DonationsView.as_view()

class GotDonationFromYandexView(View):
    """Получение события об успешной оплате от Yandex Money."""

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request):

        donation = services.YandexMoneyAPI(request).process_donation()

        if donation:
            
            mail_managers(
                subject='Новое пожертвование!',
                message=f"{donation.__str__()}")

            return HttpResponse(status=200)

got_donation_from_yandex_view = GotDonationFromYandexView.as_view()