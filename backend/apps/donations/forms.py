from django import forms

from .models import Donation
from ..utils.forms import CaptchaField


class CreateDonationForm(forms.ModelForm):
    """Форма отправки доната."""

    def __init__(self, author=None, *args, **kwargs):
        self.author = author
        super().__init__(*args, **kwargs)

    def save(self):
        self.instance.author = self.author
        super().save()

    class Meta:
        model = Donation
        fields = ('__all__')