from django.urls import path

from . import views

# Мои маршруты
urlpatterns = [
    # Главная страница
    path('', views.index, name='index'),
]