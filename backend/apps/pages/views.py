from django.views.generic import TemplateView

from ..news.models import New
from ..music.models import Release
from ..articles.models import Article


class IndexView(TemplateView):
    """Главная страница."""

    template_name = 'pages/index.html'

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs['news'] = New.activated.all()[:5]
        kwargs['releases'] = Release.activated.all().order_by('-created_at')[:8]
        kwargs['articles'] = Article.activated.all().order_by('-published_at')[:10]
        return kwargs

index = IndexView.as_view()