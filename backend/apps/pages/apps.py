from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = 'apps.pages'
    verbose_name = 'Простые страницы'
