from django.contrib import admin
from django.urls import reverse

from .models import Article
from ..contents.admin import ContentAdminMixin


@admin.register(Article)
class ArticleAdmin(ContentAdminMixin, admin.ModelAdmin):
    pass