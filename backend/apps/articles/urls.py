from django.urls import path

from . import views


urlpatterns = [

    # Статьи
    path('', views.articles_list_view, name='articles'),
    path('<int:pk>/', views.article_detail_view, name='article_detail'),

]