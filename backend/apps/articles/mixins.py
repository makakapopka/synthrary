"""
from braces.views import LoginRequiredMixin, MultiplePermissionsRequiredMixin

class PermissionWorkWithArticlesMixin(LoginRequiredMixin, MultiplePermissionsRequiredMixin):
    redirect_unauthenticated_users = True
    raise_exception = True
    permissions = {
        "all": (
            'articles.add_article',
            'articles.view_article',
            'articles.change_article',
            'articles.delete_article',

            'articles.add_category',
            'articles.view_category',
            'articles.change_category',
            'articles.delete_category',

            'articles.add_tag',
            'articles.view_tag',
            'articles.change_tag',
            'articles.delete_tag',
            ),
        }
"""