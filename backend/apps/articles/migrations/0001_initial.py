# Generated by Django 3.1.2 on 2020-11-02 20:03

import apps.utils.helpers
from django.db import migrations, models
import hitcount.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='время создания')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='время обновления')),
                ('is_active', models.BooleanField(default=False, verbose_name='запись доступна')),
                ('title', models.CharField(max_length=128, verbose_name='заголовок')),
                ('short_content', models.TextField(blank=True, max_length=300, verbose_name='краткое содержание')),
                ('content', models.TextField(blank=True, max_length=100000, verbose_name='полное содержание')),
                ('picture', models.ImageField(default='../static/img/default_article.jpg', max_length=256, upload_to=apps.utils.helpers.RandomFileNameHelper('articles/'), verbose_name='изображение')),
            ],
            options={
                'verbose_name': 'публикация',
                'verbose_name_plural': 'публикации',
                'ordering': ['-created_at'],
            },
            bases=(models.Model, hitcount.models.HitCountMixin),
        ),
    ]
