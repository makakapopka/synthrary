from django.views.generic import ListView

from hitcount.views import HitCountDetailView

from .models import Article
from .filters import ArticlesFilterSet
from ..comments.views import DetailViewCommentsPaginatorMixin


class ArticlesListView(ListView):
    """Страница со списком публикаций."""

    context_object_name = 'articles'
    paginate_by = 10
    template_name = 'articles/articles.html'
    extra_context = {'filter': ArticlesFilterSet}
    queryset = Article.activated.all()

    def get_queryset(self):
        filtered_queryset = ArticlesFilterSet(self.request.GET, queryset=self.queryset).qs
        return filtered_queryset

articles_list_view = ArticlesListView.as_view()

class ArticleDetailView(DetailViewCommentsPaginatorMixin, HitCountDetailView):
    """Страница публикации."""

    model = Article
    template_name = 'articles/article.html'
    count_hit = True

article_detail_view = ArticleDetailView.as_view()