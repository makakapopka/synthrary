from .models import Article
from ..contents.filters import ContentFilterSet

class ArticlesFilterSet(ContentFilterSet):
    """Фильтр публикаций."""

    class Meta:
        model = Article
        fields = ['title', 'category', 'tags', 'ordering']