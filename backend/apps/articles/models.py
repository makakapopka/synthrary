from django.db import models
from django.urls import reverse

from ..contents.models import BaseContentModelMixin
from ..utils.helpers import RandomFileNameHelper


class Article(BaseContentModelMixin):
    """Модель публикации."""

    picture = models.ImageField(verbose_name='изображение',
                                default='../static/img/default_article.jpg',
                                upload_to=RandomFileNameHelper('articles/'), max_length=256)

    def get_absolute_url(self):
        return reverse('article_detail', kwargs={'pk': self.pk})

    class Meta:
        verbose_name = 'публикация'
        verbose_name_plural = 'публикации'
        ordering = ['-published_at']