from django.utils.translation import ugettext_lazy as _
from jet.dashboard import modules
from jet.dashboard.dashboard import Dashboard, AppIndexDashboard
from jet.utils import get_admin_site_name
from django.urls import reverse

from django.contrib.auth import get_user_model

from apps.news.models import New
from apps.articles.models import Article
from apps.music.models import Musician, Release

from allauth.account.models import EmailAddress

User = get_user_model()

class Users(modules.DashboardModule):
    title = 'Статистика'
    template = 'jet/dashboard_modules/users.html'

    def init_with_context(self, context):
        self.users = User.objects.count()
        self.activated_users = EmailAddress.objects.exclude(verified=False).count()
        self.active_users = User.objects.exclude(is_active=False).count()

        self.news = New.objects.count()
        self.active_news = New.objects.exclude(is_active=False).count()

        self.articles = Article.objects.count()
        self.active_articles = Article.objects.exclude(is_active=False).count()

        self.musicians = Musician.objects.count()
        self.active_musicians = Musician.objects.exclude(is_active=False).count()

        self.releases = Release.objects.count()
        self.active_releases = Release.objects.exclude(is_active=False).count()

class CustomIndexDashboard(Dashboard):
    columns = 5

    def init_with_context(self, context):
        self.available_children.append(modules.LinkList)

        site_name = get_admin_site_name(context)
        # append a link list module for "quick links"
        self.children.append(modules.LinkList(
            _('Quick links'),
            layout='stacked',
            draggable=True,
            deletable=True,
            collapsible=True,
            children=[
                ['НА ГЛАВНУЮ', '/'],
                ['НОВОСТИ', reverse('news')],
                ['ПУБЛИКАЦИИ', reverse('articles')],
                ['РЕЛИЗЫ', reverse('releases')],
                ['ИСПОЛНИТЕЛИ', reverse('musicians')],
            ],
            column=0,
            order=0
        ))

        # append an app list module for "Applications"
        self.children.append(modules.AppList(
            'Общие приложения',
            models=('hitcount.*', 'comments.*'),
            column=1,
            order=0
        ))

        # append an app list module for "Administration"
        self.children.append(modules.AppList(
            _('Administration'),
            models=('auth.*', 'filebrowser.*', 'feedback.*', 'donations.*'),
            column=2,
            order=0
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            10,
            column=0,
            order=1
        ))

        self.children.append(modules.AppList(
            'Приложение: music',
            models=('music.*',),
            column=1,
            order=0
        ))

        self.children.append(modules.AppList(
            'Приложения: новости и публикации',
            models=('news.*', 'articles.*', 'categories.*', 'taggit.*'),
            column=1,
            order=0
        ))

        self.children.append(modules.AppList(
            'Авторизация',
            models=('users.*', 'socialaccount.*', 'account.*', 'sites.*'),
            column=1,
            order=0
        ))

        self.children.append(Users())