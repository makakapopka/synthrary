from django.db import models
from django.contrib.admin import widgets as admin_widgets


class TinyMCEWidget(admin_widgets.AdminTextareaWidget):
    def __init__(self, attrs=None):
        super().__init__(attrs={'class': 'tinymce', **(attrs or {})})


class TinyMCETextareaAdminMixin:
    """Виджет изменяет Textarea на Tinymce"""

    formfield_overrides = {
        models.TextField: {'widget': TinyMCEWidget},
    }

    class Media:
        js = ('js/libs/tinymce/tinymce.min.js', 'js/tinymce_init.js')
        css = {"all": ('css/tinymce/admin_labels.css', )}