import os

import magic

from django.utils.deconstruct import deconstructible
from django.core.exceptions import ValidationError


@deconstructible
class FileTypeValidator:
    """Валидатор типа файла."""

    READ_SIZE = 5 * (1024 * 1024)  # 5MB

    type_message = "Формат %(detected_type)s не разрешен. Доступные форматы: %(allowed_types)s."

    extension_message = "Расширение файла %(extension)s не разрешено. Доступные расширения: %(allowed_extensions)s."

    invalid_message = "Allowed type %(allowed_type)s is not a valid type. See https://www.iana.org/assignments/media-types/media-types.xhtml."

    def __init__(self, allowed_types, allowed_extensions=()):
        self.input_allowed_types = allowed_types
        self.allowed_mimes = self._normalize(allowed_types)
        self.allowed_exts = allowed_extensions

    def __call__(self, fileobj):
        detected_type = magic.from_buffer(fileobj.read(self.READ_SIZE), mime=True)
        root, extension = os.path.splitext(fileobj.name.lower())

        # seek back to start so a valid file could be read
        # later without resetting the position
        fileobj.seek(0)

        if detected_type not in self.allowed_mimes \
                and detected_type.split('/')[0] not in self.allowed_mimes:
            raise ValidationError(
                message=self.type_message,
                params={
                    'detected_type': detected_type.split('/')[-1].upper(),
                    'allowed_types': ', '.join(t.split('/')[-1].upper() for t in self.input_allowed_types)
                },
                code='invalid_type'
            )

        if self.allowed_exts and (extension not in self.allowed_exts):
            raise ValidationError(
                message=self.extension_message,
                params={
                    'extension': extension,
                    'allowed_extensions': ', '.join(self.allowed_exts)
                },
                code='invalid_extension'
            )

    def _normalize(self, allowed_types):
        """
        Validate and transforms given allowed types
        e.g; wildcard character specification will be normalized as text/* -> text
        """
        allowed_mimes = []
        for allowed_type in allowed_types:
            allowed_type = allowed_type.decode() if type(allowed_type) is bytes else allowed_type
            parts = allowed_type.split('/')
            if len(parts) == 2:
                if parts[1] == '*':
                    allowed_mimes.append(parts[0])
                else:
                    allowed_mimes.append(allowed_type)
            else:
                raise ValidationError(
                    message=self.invalid_message,
                    params={
                        'allowed_type': allowed_type
                    },
                    code='invalid_input'
                )

        return allowed_mimes


class FileWeightValidator:
    """Валидатор веса файла."""

    def __init__(self, weight):
        self.weight = weight

    def __call__(self, fileobj):
        fileobj.seek(0)
        fileobj_weight = fileobj.size

        if fileobj_weight > self.weight:
            raise ValidationError(
                message=f'Изображение должно быть не больше {self.weight/(1024 * 1024):0.0f} МБ, сейчас {fileobj_weight/(1024 * 1024):0.2f} МБ.',
                code='invalid_weight'
            )