from django.db import models

from .managers import ActivatedManager


class TimeStampMixin(models.Model):

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='время обновления')

    class Meta:
        abstract = True


class IsActiveModelMixin(models.Model):

    is_active = models.BooleanField(verbose_name='запись доступна', default=False)

    objects = models.Manager()
    activated = ActivatedManager()

    class Meta:
        abstract = True