from django.db import models

class ActivatedManager(models.Manager):
    """Менеджер для обращения только к активным записям модели."""

    def get_queryset(self):
        return super().get_queryset().exclude(is_active=False)