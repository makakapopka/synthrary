import django_filters
from taggit.forms import TagField
from taggit.utils import parse_tags


class TagsFilter(django_filters.CharFilter):
    field_class = TagField

    def filter_tags(self, queryset, name, value):
        lookup = '%s__%s' % ('tags__name', 'in')
        value = [obj.lower().replace('#', '') for obj in value]
        return queryset.filter(**{lookup: value}).distinct()

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('method', self.filter_tags)
        super().__init__(*args, **kwargs)