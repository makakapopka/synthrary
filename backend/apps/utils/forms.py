from django import forms
from django.contrib.admin.forms import (
    AdminAuthenticationForm as _AdminAuthenticationForm
)

from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox


class AddFormMixin:
	"""Переорпеделяет форму добавления нового обьекта в админке,
		чтобы можно было создать без картинки."""

	def get_form(self, request, obj=None, **kwargs):
		defaults = {}
		if obj is None:
			defaults['form'] = self.add_form
		defaults.update(kwargs)
		return super().get_form(request, obj, **defaults)


CaptchaField = ReCaptchaField(widget=ReCaptchaV2Checkbox(api_params={'hl': 'ru'}),
							  error_messages={
							  	'required': 'Подтвердите, что вы не робот.', 
							 		"captcha_invalid": "Ошибка проверки reCaptcha, пожалуйста попробуйте еще раз.",
        					"captcha_error": "Ошибка проверки reCaptcha, пожалуйста попробуйте еще раз."}, 
        				label='')

class CaptchaForm(forms.Form):
	captcha = CaptchaField

class AdminAuthenticationForm(_AdminAuthenticationForm):
    captcha = CaptchaField