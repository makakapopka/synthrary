from ..utils.widgets import TinyMCETextareaAdminMixin
from . import forms
from ..utils.forms import AddFormMixin


class ContentAdminMixin(AddFormMixin, TinyMCETextareaAdminMixin):
    add_form = forms.ContentCreationForm
    form = forms.CleanedPictureChangeForm

    list_display = ('id', 'created_at', 'published_at', 'author', 'title', 'category', 'hitcounts', 'is_active')
    list_display_links = ('created_at', 'author', 'title')
    list_editable = ('is_active', )
    ordering = ('-created_at', '-published_at', 'author', 'title', 'category', 'is_active')
    sortable_by = ('created_at', 'published_at', 'author', 'title', 'category', 'is_active')
    search_fields = ('author', 'title', 'category', 'short_content')
    list_filter = ('created_at', 'published_at', 'author', 'title', 'category', 'is_active')
    date_hierarchy = 'published_at'
    raw_id_fields = ('author', 'category')

    def hitcounts(self, obj):
        return obj.hit_count.hits
    hitcounts.short_description = 'Количество просмотров'

    def view_on_site(self, obj):
        return obj.get_absolute_url()

    fieldsets = (
        ('Общее', {
            'fields': ('author', 'title', 'picture', 'category',
                        'tags', 'published_at', 'is_active')
            }
         ),

        ('Краткое содержание', {
            'fields': ('short_content',)
        }),

        ('Полное содержание', {
            'fields': ('content',)
        })
    )