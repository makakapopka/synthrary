from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from django.utils import timezone

from taggit.managers import TaggableManager
from hitcount.models import HitCountMixin

from ..users.models import User
from ..utils.models import TimeStampMixin, IsActiveModelMixin
from ..categories.models import Category


class BaseContentModelMixin(TimeStampMixin, IsActiveModelMixin, HitCountMixin):
    """Миксин модели, содержащей текстовый контент (новости/статьи)"""

    author = models.ForeignKey(User, verbose_name='автор',
                               on_delete=models.SET_NULL, blank=True, null=True)

    title = models.CharField(verbose_name='заголовок', max_length=128)

    published_at = models.DateTimeField(verbose_name='время публикации', default=timezone.now)

    short_content = models.TextField(verbose_name='краткое содержание',
                                     max_length=300, blank=True)

    content = models.TextField(verbose_name='полное содержание',
                               max_length=100000, blank=True)

    category = models.ForeignKey(Category, verbose_name='категория',
                                 blank=True, null=True, on_delete=models.SET_NULL)
    tags = TaggableManager(verbose_name='тэги', blank=True)

    comments = GenericRelation('comments.comment')

    def view_on_site(self, obj):
        return obj.get_absolute_url()

    def __str__(self):
        return self.title

    class Meta:
        abstract = True