import django_filters

from ..categories.models import Category
from ..utils.filters import TagsFilter

class ContentFilterSet(django_filters.FilterSet):
    """Фильтр контента."""

    title = django_filters.CharFilter(lookup_expr='icontains')

    category = django_filters.ModelChoiceFilter(
        queryset=Category.objects.all(), empty_label='Все категории')

    tags = TagsFilter()

    ordering = django_filters.OrderingFilter(
        fields=(('created_at', '-created_at'), ('created_at', 'created_at'))
    )