import io

from django import forms
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import UploadedFile
from django.db.models.fields.files import ImageFieldFile

from PIL import Image, ImageOps

from ..utils.validators import FileTypeValidator, FileWeightValidator
from ..articles.models import Article


class ContentCreationForm(forms.ModelForm):
    """Форма создания записи контента в админке."""

    picture = forms.ImageField(label='Изображение',
        validators=[FileTypeValidator(allowed_types=['image/jpg', 'image/jpeg', 'image/png']),
        FileWeightValidator(weight=2*(1024*1024))], required=False)

    def clean_picture(self):
        pic = self.cleaned_data.get('picture', None)
        if pic is not None and isinstance(pic, UploadedFile):
            root, ext = pic.content_type.split('/')
            picture = Image.open(pic)
            picture = ImageOps.fit(picture, (1280, 720), Image.ANTIALIAS, centering=(0.0, 0.0))
            picture_io = io.BytesIO()
            picture.save(picture_io, format=ext)
            picture = ContentFile(picture_io.getvalue(), pic.name)
            return picture
        elif pic is not None and isinstance(pic, ImageFieldFile):
            pass


class CleanedPictureChangeForm(forms.ModelForm):
    """Форма подготовки и валидации изображения контента в админке."""

    picture = forms.ImageField(label='Изображение',
        validators=[FileTypeValidator(allowed_types=['image/jpg', 'image/jpeg', 'image/png']),
        FileWeightValidator(weight=2*(1024*1024))], required=False)

    def clean_picture(self):
        pic_clear = self.data.get('picture-clear', None)

        if pic_clear:
            if isinstance(self.instance, Article):
                self.instance.picture = '../static/img/default_article.jpg'
                self.instance.save()
            else:
                self.instance.picture = None
                self.instance.save()

        pic = self.cleaned_data.get('picture', None)
        if pic is not None and isinstance(pic, UploadedFile):
            root, ext = pic.content_type.split('/')
            picture = Image.open(pic)
            picture = ImageOps.fit(picture, (1280, 720), Image.ANTIALIAS, centering=(0.0, 0.0))
            picture_io = io.BytesIO()
            picture.save(picture_io, format=ext)
            picture = ContentFile(picture_io.getvalue(), pic.name)
            return picture
        elif pic is not None and isinstance(pic, ImageFieldFile):
            pass