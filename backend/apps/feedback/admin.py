from django.contrib import admin

from .models import Feedback


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
	list_display = ('id', '__str__', 'created_at', 'author', 'subject', 'is_processed')
	list_display_links = ('__str__', )
	list_editable = ('is_processed', )
	ordering = ('-created_at', )
	sortable_by = ('created_at', 'author', 'subject', 'is_processed')
	search_fields = ('author', 'text')
	list_filter = ('created_at', 'subject', 'is_processed')
	readonly_fields = ('author', 'subject', 'email', 'text')