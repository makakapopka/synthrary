from django.db import models
from django.utils.timezone import make_naive

from ..utils.models import TimeStampMixin
from ..users.models import User
from ..users.validators import email_validator


class Feedback(TimeStampMixin):
	author = models.ForeignKey(
		User, verbose_name='автор',
		on_delete=models.SET_NULL, blank=True, null=True)

	SUBJECTS = (
		('common_questions', 'Общие вопросы'),
		('site_errors', 'Ошибка в работе сайта'),
		('site_vulnerability', 'Уязвимость на сайте'),
		('site_rules', 'Нарушение правил сайта'),
		('account_block', 'Блокировка аккаунта'),
		('account_change_data', 'Смена данных (имя пользователя или email)'),
		('account_delete', 'Удаление аккаунта'),
		('idea', 'Гениальная идея'),
		('offer', 'Предложение сотрудничества'),
	)

	subject = models.CharField(verbose_name='тема обращения', choices=SUBJECTS, max_length=20)
	email = models.EmailField(verbose_name='электронная почта отправителя', validators=[email_validator])
	text = models.TextField(verbose_name='текст обращения', max_length=10000)

	is_processed = models.BooleanField(verbose_name='заявка обработана', default=False)

	class Meta:
		verbose_name = "Обратная связь"
		verbose_name_plural = "Заявки"

	def __str__(self): 
		return f"{make_naive(self.created_at).strftime('%d/%m/%Y, %H:%M')} | {self.get_subject_display()}"
