from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import mail_managers

from .models import Feedback
from .forms import CreateFeedbackForm


class CreateFeedbackView(SuccessMessageMixin, CreateView):
    """Форма обратной связи."""

    model = Feedback
    template_name = 'pages/feedback.html'
    success_message = 'Ваша заявка была успешно отправлена!'
    form_class = CreateFeedbackForm

    def get_success_url(self):
        return reverse_lazy('feedback')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.user.is_authenticated:
            kwargs['author'] = self.request.user
        return kwargs

    def form_valid(self, form):
        self.object = form.save()

        mail_managers(
            subject='Новая заявка из формы обратной связи',
            message=f"""
                {form.instance.__str__()}
                Email для ответа: {form.instance.email}
                Текст обращения: {form.instance.text}
            """)

        return super().form_valid(form)


feedback = CreateFeedbackView.as_view()