from django import forms

from .models import Feedback
from ..utils.forms import CaptchaField


class CreateFeedbackForm(forms.ModelForm):
    """Форма заявки обратной связи."""

    captcha = CaptchaField

    def __init__(self, author=None, *args, **kwargs):
        self.author = author
        super().__init__(*args, **kwargs)

    def save(self):
        self.instance.author = self.author
        super().save()

    class Meta:
        model = Feedback
        fields = ('author', 'subject', 'email', 'text', 'captcha')