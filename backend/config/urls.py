from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static

from filebrowser.sites import site as fileblowser_site

from .settings import settings
from apps.utils.forms import AdminAuthenticationForm

urlpatterns = [
    path('jet/', include('jet.urls', 'jet')),
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    path('manage/filebrowser/', fileblowser_site.urls),
    path('manage/', admin.site.urls),
    path('', include('apps.users.urls')),
    path('', include('apps.pages.urls')),
    path('music/', include('apps.music.urls')),
    path('comments/', include('apps.comments.urls')),
    path('news/', include('apps.news.urls')),
    path('articles/', include('apps.articles.urls')),
    path('feedback/', include('apps.feedback.urls')),
    path('donations/', include('apps.donations.urls')),
]

admin.site.site_header = 'Администрирование Synthrary'
admin.site.site_title = 'Администрирование Synthrary'
admin.site.index_title = 'Synthrary'
admin.site.login_form = AdminAuthenticationForm


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    import debug_toolbar
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]