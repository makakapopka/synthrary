"""Django 3.1.2"""

import os

# PATHS
# ------------------------------------------------------------------------------
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
ROOT_DIR = os.path.realpath(os.path.join(BASE_DIR, '..', '..'))

# Frontend and backend paths
BACKEND_DIR = os.path.realpath(os.path.join(BASE_DIR, '..'))
# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
FRONTEND_DIR = os.path.join(ROOT_DIR, 'frontend')


# GENERAL
# ------------------------------------------------------------------------------
# Local time zone. Choices are
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# though not all of them may be available with every OS.
# In Windows, this must be set to your system time zone.
TIME_ZONE = 'Asia/Yekaterinburg'
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'ru-ru'
# https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True
# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1


# URLS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = "config.urls"
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = "config.wsgi.application"
# LOGIN AND LOGOUT


# APPS
# ------------------------------------------------------------------------------
INSTALLED_APPS = [

    #должны быть сверху
    'filebrowser',
    'jet.dashboard',
    'jet',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'django.contrib.humanize',

    # СТОРОННИЕ ПРИЛОЖЕНИЯ
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    "anymail",
    'django_filters',
    'taggit',
    'mptt',
    'hitcount',
    'captcha',

    # ЛОКАЛЬНЫЕ ПРИЛОЖЕНИЯ
	'apps.users',
    'apps.comments',
    'apps.music',
    'apps.categories',
    'apps.news',
    'apps.articles',
    'apps.utils',
    'apps.feedback',
    'apps.donations',

    'django_cleanup.apps.CleanupConfig' # должен быть в конце

]


# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get("DB_NAME"),
        'USER': os.environ.get("DB_USER"),
        'PASSWORD': os.environ.get("DB_PASSWORD"),
        'HOST': os.environ.get("DB_HOST"),
        'PORT': os.environ.get("DB_PORT")
    }
}


# AUTHENTICATION
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
]
AUTH_USER_MODEL = 'users.User'
LOGIN_REDIRECT_URL = '/'
ACCOUNT_LOGOUT_REDIRECT_URL = '/'
LOGIN_URL = 'account_login'


# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',},
]


# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


# STATIC
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = "/static/"
# https://docs.djangoproject.com/en/dev/ref/settings/#staticfiles-dirs
STATICFILES_DIRS = [
    os.path.join(FRONTEND_DIR, 'dist/static/')
]
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = os.path.join(BACKEND_DIR, 'resources/static/')


# MEDIA
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = os.path.join(BACKEND_DIR, 'resources/media/')
# https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = "/media/"


# SECURITY
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-httponly
SESSION_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-httponly
CSRF_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-browser-xss-filter
SECURE_BROWSER_XSS_FILTER = True
# https://docs.djangoproject.com/en/dev/ref/settings/#x-frame-options
X_FRAME_OPTIONS = "SAMEORIGIN"


# ADMIN
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = [('tyske', 'tyske@yahoo.com')]
# https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS


# django-allauth
# https://django-allauth.readthedocs.io/en/latest/configuration.html
# ------------------------------------------------------------------------------
ACCOUNT_AUTHENTICATION_METHOD = "username_email"
ACCOUNT_USERNAME_MIN_LENGTH = 3
ACCOUNT_EMAIL_VERIFICATION = "mandatory"
ACCOUNT_EMAIL_CONFIRMATION_HMAC = False
ACCOUNT_CONFIRM_EMAIL_ON_GET = True
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 1
ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = 'account_settings'
ACCOUNT_EMAIL_SUBJECT_PREFIX = 'Synthrary | '
ACCOUNT_USER_MODEL_EMAIL_FIELD = 'email'
ACCOUNT_USER_MODEL_USERNAME_FIELD = 'username'
ACCOUNT_USERNAME_REQUIRED = True
ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_FORMS = {
    'signup': 'apps.users.forms.CustomSignupForm',
    'login': 'apps.users.forms.CustomLoginForm',
    'reset_password': 'apps.users.forms.CustomPasswordResetForm'
}
ACCOUNT_ADAPTER = 'apps.users.adapters.CustomDefaultAccountAdapter'


# EMAIL and django-anymail
# https://anymail.readthedocs.io/en/stable/quickstart/#
# ------------------------------------------------------------------------------
SERVER_EMAIL = "notify@synthrary.ru"  # ditto (default from-email for Django errors)
# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-EMAIL_SUBJECT_PREFIX
EMAIL_SUBJECT_PREFIX = 'Synthrary уведомление | '
ANYMAIL_MAILGUN_API_KEY = os.environ.get("MAILGUN_API_KEY", "")
EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"
DEFAULT_FROM_EMAIL = "noreply@synthrary.ru"  # if you don't already have this in settings


# django-jet
# https://jet.readthedocs.io/en/latest/index.html
# ------------------------------------------------------------------------------
JET_DEFAULT_THEME = 'green'
JET_THEMES = [
    {
        'theme': 'default', # theme folder name
        'color': '#47bac1', # color of the theme's button in user menu
        'title': 'Default' # theme title
    },
    {
        'theme': 'green',
        'color': '#44b78b',
        'title': 'Green'
    },
    {
        'theme': 'light-green',
        'color': '#2faa60',
        'title': 'Light Green'
    },
    {
        'theme': 'light-violet',
        'color': '#a464c4',
        'title': 'Light Violet'
    },
    {
        'theme': 'light-blue',
        'color': '#5EADDE',
        'title': 'Light Blue'
    },
    {
        'theme': 'light-gray',
        'color': '#222',
        'title': 'Light Gray'
    }
]
JET_INDEX_DASHBOARD = 'apps.dashboard.CustomIndexDashboard'


# django-filebrowser
# https://django-filebrowser.readthedocs.io/en/latest/settings.html
# ------------------------------------------------------------------------------
FILEBROWSER_DIRECTORY = ""
FILEBROWSER_VERSIONS = {
    'admin_thumbnail': {'verbose_name': 'Миниатюра для админки', 'width': 60, 'height': 60, 'opts': 'crop'},
}
FILEBROWSER_ADMIN_VERSIONS = ['admin_thumbnail']
FILEBROWSER_ADMIN_THUMBNAIL = 'admin_thumbnail'
FILEBROWSER_PLACEHOLDER = os.path.join(MEDIA_ROOT, 'logopicture.jpg')
FILEBROWSER_SHOW_PLACEHOLDER = True
FILEBROWSER_NORMALIZE_FILENAME = True


# django-taggit
# https://django-taggit.readthedocs.io/en/latest/getting_started.html
# ------------------------------------------------------------------------------
TAGGIT_CASE_INSENSITIVE = True


# django-recaptcha
# https://github.com/praekelt/django-recaptcha
# ------------------------------------------------------------------------------
RECAPTCHA_PUBLIC_KEY = os.environ.get("RECAPTCHA_PUBLIC_KEY")
RECAPTCHA_PRIVATE_KEY = os.environ.get("RECAPTCHA_PRIVATE_KEY")

# Yandex Money (ЮMoney)
# ------------------------------------------------------------------------------
YANDEX_MONEY_SECRET_KEY = os.environ.get("YANDEX_MONEY_SECRET_KEY")