var	applicationName = 'Synthray',
		syntax        	= 'sass', // Syntax: sass or scss
		managePath    	= '../backend/manage.py';

var gulp          = require('gulp'),
		gutil         = require('gulp-util' ),
		sass          = require('gulp-sass'),
		browserSync   = require('browser-sync'),
		concat        = require('gulp-concat'),
		uglify        = require('gulp-uglify'),
		cleancss      = require('gulp-clean-css'),
		rename        = require('gulp-rename'),
		autoprefixer  = require('gulp-autoprefixer'),
		notify        = require('gulp-notify'),
		filesize      = require('gulp-filesize'),
		exec          = require('child_process').exec,
		reload        = browserSync.reload,
		del						= require('gulp-clean'),
		imagemin      = require('gulp-imagemin'),
		cache 				= require('gulp-cached')
		;

// Compile Sass to Css
gulp.task('sass', function() {
	return gulp.src('app/static/sass/**/*.sass')
	.pipe(cache('linting'))
	.pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleancss( {level: { 1: { specialComments: 0 } } }))
	.pipe(filesize())
	.pipe(gulp.dest('dist/static/css'))
	.pipe(browserSync.stream())
});

// // Compile Django-JET Scss to Css
// gulp.task('jet', function() {
// 	return gulp.src('app/static/sass/jet/**/*.scss')
// 	.pipe(cache('linting'))
// 	.pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
// 	.pipe(autoprefixer(['last 15 versions']))
// 	.pipe(cleancss( {level: { 1: { specialComments: 0 } } }))
// 	.pipe(gulp.dest('dist/static/jet'))
// });

// Compile Sass and Scss to Css
gulp.task('styles', ['sass'])




// Compile my common JS scripts
gulp.task('common-js', function() {
	return gulp.src([
		'app/static/js/common.js',
		])
	.pipe(cache('linting'))
	.pipe(concat('common.min.js'))
	.pipe(uglify().on('error', function(e){
      console.log(e);
   }))
	.pipe(gulp.dest('app/static/js'))
	.pipe(filesize());
});

// Не компилируемые библиотеки просто перенести в папку с JS проекта
gulp.task('jslibs', function () {
	return gulp.src([
		'app/static/libs/tinymce/js/**/*',
		])
		.pipe(cache('linting'))
    .pipe(gulp.dest('dist/static/js/libs'))
    .pipe(reload({ stream: true }))
    .pipe(filesize())
});

// Отдельные скрипты
gulp.task('otherjs', function() {
	return gulp.src([
		'app/static/js/tinymce_init.js',
		'app/static/js/donations.js',
		'app/static/js/header.js'
		])
		.pipe(cache('linting'))
		.pipe(uglify().on('error', function(e){
		    console.log(e);
		 }))
    .pipe(gulp.dest('dist/static/js'))
    .pipe(reload({ stream: true }))
    .pipe(filesize())
});

// Работа с JS
gulp.task('js', ['common-js', 'jslibs', 'otherjs'], function() {
	return gulp.src([
		// мои библиотеки
		'app/static/libs/jquery/dist/jquery.min.js',
		'app/static/libs/bootstrap/js/tab.js',
		'app/static/libs/bootstrap/js/alert.js',
		'app/static/libs/bootstrap/js/transition.js',
		'app/static/libs/fontawesome/all.min.js',
		'app/static/libs/owl-carousel/dist/owl.carousel.min.js',
		'app/static/libs/mheader/mhead.js',
		'app/static/libs/mmenu/dist/mmenu.js',
		'app/static/libs/select2/dist/js/select2.min.js',
		'app/static/libs/aos/dist/aos.js',
		'app/static/js/common.min.js', // Всегда в конце
		])
	.pipe(concat('scripts.min.js'))
	.pipe(gulp.dest('dist/static/js'))
	.pipe(filesize())
	.pipe(reload({ stream: true }))
});




// Copy Web Fonts To Dist
gulp.task('fonts', function() {
  return gulp.src(['app/static/fonts/**/*'])
  	.pipe(cache('linting'))
    .pipe(gulp.dest('dist/static/fonts'))
});

// From app images to dist images
gulp.task('img', () =>
  gulp.src('app/static/img/**/*')
			.pipe(cache('linting'))
      .pipe(imagemin())
      .pipe(gulp.dest('dist/static/img'))
      .pipe(reload({ stream: true }))
      .pipe(filesize())
);


// Watch HTML Files
gulp.task('html', function() {
	return gulp.src('app/templates/**/*.+(html|txt)')
	.pipe(cache('linting'))
	.pipe(gulp.dest('dist/templates'))
	.pipe(reload({ stream: true }))
});

// Running Django Server
gulp.task('runserver', function() {
  var proc = exec('py -3 '+managePath+' runserver');
  proc.stderr.on('data', function(data) {
    process.stdout.write(data);
  });

  proc.stdout.on('data', function(data) {
    process.stdout.write(data);
  });
});

gulp.task('browser-sync', function() {
	browserSync({
		notify: false,
		open: false,
		port: 8000,
		proxy: 'localhost:8000',
		// online: false, // Work Offline Without Internet Connection
		// tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
	})
});


gulp.task('default', ['styles', 'js', 'html', 'fonts', 'img', 'browser-sync'], function() {
	gulp.watch('app/static/'+syntax+'/**/*', ['styles', reload]);
	gulp.watch(['app/static/libs/**/*', 'app/static/js/**/*.js'], ['js', reload]);
	gulp.watch('app/templates/**/*.html', ['html', reload]);
	gulp.watch('app/static/fonts/**/*', ['fonts', reload]);
	gulp.watch('app/static/img/**/*', ['img', reload]);
});

gulp.task('fast', ['browser-sync'], function() {
	gulp.watch('app/static/'+syntax+'/**/*', ['styles', reload]);
	gulp.watch(['app/static/libs/**/*', 'app/static/js/**/*.js'], ['js', reload]);
	gulp.watch('app/templates/**/*.html', ['html', reload]);
	gulp.watch('app/static/fonts/**/*', ['fonts', reload]);
	gulp.watch('app/static/img/**/*', ['img', reload]);
})

// Clean dist
gulp.task('clean', function() { return gulp.src('dist', {read: false}).pipe(del()); });

//gulp.task('build', ['styles', 'js', 'html', 'fonts', 'img'], function() {
	//return gulp.src()
//});