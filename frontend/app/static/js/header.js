$(window).on("scroll touchmove", function() {
  header = $('.header');
  content = $('.content');
  if ( $(document).scrollTop() >= 131 && $( window ).width() >= 992 ) {
      header.addClass('header_fixed');
      content.css('margin-top', "131px")
  } else {
      header.removeClass('header_fixed');
      content.css('margin-top', 0)
    }
});