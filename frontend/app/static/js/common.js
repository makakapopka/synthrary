

$(window).on('load', function () {
  var body = $('body');
  body.addClass('loaded_hiding');
  $('.preloader').delay(350);
  body.addClass('loaded');
  body.removeClass('loaded_hiding');
  body.css('overflow-y', 'auto');
});

$(function() {
    $(".owl-releases").owlCarousel({
        loop: true,
        margin: 30,
        autoplay: true,
        autoplaySpeed: 2500,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            1200: {
                items: 4
            }
        }

    });
});

$(function() {

    var menu = $(".header__usermenu")
    $(".header__usermenu-button").click(function() {
      $(".header__usermenu").toggleClass("dropdown_active");
    })

  $(document).on('click', function(e) {
    if (!$(e.target).closest(menu).length) {
      menu.removeClass("dropdown_active");
    }
    e.stopPropagation();
  });

});

AOS.init({
  // Global settings:
  disable: 'phone', // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
  startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
  initClassName: 'aos-init', // class applied after initialization
  animatedClassName: 'aos-animate', // class applied on animation
  useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
  disableMutationObserver: false, // disables automatic mutations' detections (advanced)
  debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
  throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
  

  // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
  offset: 120, // offset (in px) from the original trigger point
  delay: 0, // values from 0 to 3000, with step 50ms
  duration: 400, // values from 0 to 3000, with step 50ms
  easing: 'ease', // default easing for AOS animations
  once: true, // whether animation should happen only once - while scrolling down
  mirror: false, // whether elements should animate out while scrolling past them
  anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation

});

$(function () {
  window.addEventListener('load', AOS.refresh)
})

function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('.edit-picture')
          .attr('src', e.target.result);
      };

      reader.readAsDataURL(input.files[0]);
  }
}

$(function() {
  $('.select').select2({
    minimumResultsForSearch: Infinity,
    placeholder: function(){
        $(this).data('placeholder');
    },

    language: {
      noResults: function() {return"Совпадений не найдено"}
      }
  });

  $('.select_search').select2({
    placeholder: function(){
        $(this).data('placeholder');
    },
    language: {
      noResults: function() {return"Совпадений не найдено"}
      }
  });
});


$(function() {

    $(".collapse-button").click(function() {
        $(".collapse").toggleClass('show');
    })

});

$(function indexHeader () {
  $(window).on("scroll touchmove", function() {
  header = $('.header');
  content = $('.content');
  if ( $(document).scrollTop() >= 131 && $( window ).width() >= 992 ) {
      header.addClass('header_fixed');
  } else {
      header.removeClass('header_fixed');
    }
  });
});

$(function() {
  new Mhead('.header-mobile', {
      pin: 300,
      unpin: 500,
      tolerance: 1
    });
});

$(function() {

  new Mmenu("#mheader-menu", {

    hooks: {

      "open:finish": function() {
        $('#mheader-hamburger').addClass('is-active');
        $('#mheader-menu').removeClass('d-none');
      },

      "close:start": function() {
        $('#mheader-menu').addClass('d-none');
      },

      "close:finish": function() {
        $('#mheader-hamburger').removeClass('is-active');
      },

    },

    // configuration
    offCanvas: {
      page: {
        nodetype: "#page"
      }
    },

    backButton: {
      close: true
    },

    navbar: {
      title: 'Synthrary',
    },

    "setSelected": {
      "hover": true
    },

    extensions: ['theme-white', 'border-full', 'pagedim-black'],

  });

  $('#mheader-menu').addClass('d-none');

});

$(function() {

  new Mmenu("#mheader-usermenu", {

    hooks: {

      "open:finish": function() {
        $('#mheader-usermenu').removeClass('d-none');
      },

      "close:start": function() {
        $('#mheader-usermenu').addClass('d-none');
      },

    },

    // configuration
    offCanvas: {
      page: {
        nodetype: "#page"
      }
    },

    backButton: {
      close: true
    },

    navbar: {
      title: 'Аккаунт',
    },

    "setSelected": {
      "hover": true
    },

    extensions: ['theme-white', 'border-full', 'position-right', 'pagedim-black']

  });

  $('#mheader-usermenu').addClass('d-none');

});

(function($) {
  $.fn.shorten = function (settings) {
  
    var config = {
      showChars: 100,
      moreText: "Показать полностью",
      lessText: "Скрыть"
    };

    if (settings) {
      $.extend(config, settings);
    }
    
    $(document).off("click", '.morelink');
    
    $(document).on({click: function () {

        var $this = $(this);
        if ($this.hasClass('less')) {
          $this.removeClass('less');
          $this.html(config.moreText);
        } else {
          $this.addClass('less');
          $this.html(config.lessText);
        }
        $this.parent().prev().toggle();
        $this.prev().toggle();
        return false;
      }
    }, '.morelink');

    return this.each(function () {
      var $this = $(this);
      if($this.hasClass("shortened")) return;
      
      $this.addClass("shortened");
      var content = $this.html();
      if (content.length > config.showChars) {
        var c = content.substr(0, config.showChars);
        var h = content.substr(config.showChars, content.length - config.showChars);
        var html = c + '<span class="moreellipses">...</span><span class="morecontent"><span>' + h + '</span> <a href="#" class="morelink">' + config.moreText + '</a></span>';
        $this.html(html);
        $(".morecontent span").hide();
      }
    });
    
  };

 })(jQuery);

$(function() {
  
  $(".hide-text-300").shorten({
    "showChars" : 300,
  });
  
});

// $(function() {

//   $('.comment__reply-link').on('click', function(e) {
//     var commentId = $(this).attr('data-reply-comment-id');
//     $("#reply-form-"+commentId).fadeToggle();
//     e.preventDefault();
//   });

// });