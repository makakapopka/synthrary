$(function () {
	$("#donate-button").on('click', function (e) {
		if ($("#id_payment-provider").val() == 'yandex_money') {

			var amount = parseInt($('#id_amount').val());
			if (amount < 10) {
				amount = 10
			}
			$("#yandex-money_sum").val(amount);

			$("#yandex-money_comment").val($('#id_message').val());

			$("#yandex-money_submit").trigger('click');

		}
		e.preventDefault();
	});

	$("#id_payment-provider").change(function() {
  		if ($("#id_payment-provider").val() == 'bankdetails') {
  			$(".donations-form__row_payment-bankdetails").fadeIn();
  			$("#donate-button").hide();
  		} else {
  			$(".donations-form__row_payment-bankdetails").hide();
  			$("#donate-button").show();
  		}
	}).change();
})