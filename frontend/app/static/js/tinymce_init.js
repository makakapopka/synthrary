// function CustomFileBrowser(field_name, url, type, win) {
    
//     var cmsURL = '/admin/filebrowser/browse/?pop=4/';
//     cmsURL = cmsURL + '&type=' + type;
    
//     tinymce.activeEditor.windowManager.open({
//         file: cmsURL,
//         width: 980,  // Your dimensions may differ - toy around with them!
//         height: 500,
//         resizable: 'yes',
//         scrollbars: 'yes',
//         inline: 'no',  // This parameter only has an effect if you use the inlinepopups plugin!
//         close_previous: 'no'
//     }, {
//         window: win,
//         input: field_name,
//         editor_id: tinymce.selectedInstance.editorId
//     });
//     return false;
// }

tinymce.init({
  selector: '.tinymce',
  plugins: 'preview save print paste image link lists searchreplace autolink link media template codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists wordcount noneditable charmap fullscreen code ',
  toolbar: 'undo redo | formatselect fontselect fontsizeselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | preview code fullscreen | forecolor backcolor removeformat | pagebreak | charmap save image media link template anchor codesample',
  contextmenu: false,
  content_css: "/static/css/tinymce/tinymce_base.css",
  placeholder: 'Текст',
  statusbar: false,
  elementpath: false,
  width: 'auto',
  height: 380,
  min_height: 380,
  language: 'ru',
  branding: false,
  extended_valid_elements : "script[*]",
  paste_as_text: true,
  forced_root_block: 'p',
  fontsize_formats: "8px 9px 10px 11px 12px 14px 16px 18px 20px 22px 24px 26px 28px 36px 48px 72px",

  // // Callbacks
  //   file_browser_callback: 'CustomFileBrowser',
});